package pt.peddavid.yamdb;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MSSQLServerContainer;
import kotlin.Pair;
import pt.peddavid.yamdb.commands.*;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.*;
import pt.peddavid.yamdb.util.Page;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

import static java.util.stream.Collectors.toList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandsTest {

    private static LocalDate today = LocalDate.now(ZoneId.ofOffset("GMT", ZoneOffset.ofHours(0)));
    private static ConnectionService service;
    @RegisterExtension
    public static MSSQLServerContainer container = new MSSQLServerContainer();

    @BeforeAll
    public static void setUp() throws SQLException {
        container.start();
        service = new MSSQLService(container.getJdbcUrl(), container.getUsername(), container.getPassword());
        service.transaction(trans -> {
            //Drop tables if exist
            trans.batch(new String[]{
                    "if object_id('dbo.Review', 'U') is not null " +
                            "begin drop table dbo.Review end",
                    "if object_id('dbo.Rating', 'U') is not null " +
                            "begin drop table dbo.Rating end",
                    "if object_id('dbo.CollectionsMovie', 'U') is not null " +
                            "begin drop table dbo.CollectionsMovie end",
                    "if object_id('dbo.Collections', 'U') is not null " +
                            "begin drop table dbo.Collections end",
                    "if object_id('dbo.Movie', 'U') is not null " +
                            "begin drop table dbo.Movie end"
            }).execute();
            //Create tables if don't exist
            trans.batch(new String[]{
                    "if object_id('dbo.Movie', 'U') is null " +
                            "begin create Table dbo.Movie(" +
                            "id int IDENTITY(1,1) primary key, " +
                            "Title varchar(50) NOT NULL, " +
                            "ReleaseYear smallint NOT NULL, " +
                            "AddedDate date CONSTRAINT DF_Orders_date default getdate(), " +
                            "constraint Title_Year_Constraint unique (Title,ReleaseYear) " +
                            ") end",
                    "if object_id('dbo.Rating', 'U') is null " +
                            "begin create Table dbo.Rating(" +
                            "id int IDENTITY(1,1), " +
                            "MovieId int foreign key references dbo.Movie, " +
                            "Score int, " +
                            "constraint PK_Constraint primary key (id, MovieID), " +
                            "constraint Score_Constraint check (Score in (1,2,3,4,5))" +
                            ") end",
                    "if object_id('dbo.Review', 'U') is null " +
                            "begin create Table dbo.Review(" +
                            "id int, " +
                            "MovieId int, " +
                            "constraint FK_Constraint foreign key (id, MovieId) references dbo.Rating, " +
                            "constraint PK_Constraint_Review primary key (id, MovieId), " +
                            "Summary varchar(100) not null, " +
                            "Name varchar(50) Not null, " +
                            "FullReview varchar(500) not null " +
                            ") end",
                    "if object_id('dbo.Collections', 'U') is null " +
                            "begin create Table dbo.Collections(" +
                            "id int IDENTITY(1,1) primary key, " +
                            "Name varchar(50) not null, " +
                            "description varchar(500) not null, " +
                            "constraint Unique_constraint unique (Name, description)" +
                            ") end",
                    "if object_id('dbo.CollectionsMovie', 'U') is null " +
                            "begin create Table dbo.CollectionsMovie(" +
                            "MovieId int, " +
                            "CollectionsId int, " +
                            "constraint FK_ConstraintC foreign key (MovieId) references dbo.Movie, " +
                            "constraint FK_ConstraintCollections foreign key (CollectionsId) " +
                            "references dbo.Collections, " +
                            "constraint PK_ConstraintC primary key (MovieId, CollectionsId) " +
                            ") end"
            }).execute();
            //Populate tables
            return trans.batch(new String[]{
                    "insert into dbo.Movie (Title,ReleaseYear) values ('Django',2012)",
                    "insert into dbo.Rating(Score,MovieId) values (4,1), (5,1), (3,1), (4,1),(4,1), (5,1)",
                    "insert into dbo.Review values (1,1,'Muito Bonito','Antonio','Muito Bonito Mesmo')",

                    "insert into dbo.Movie (Title,ReleaseYear) values ('Shutter Island',2010)",
                    "insert into dbo.Rating(Score,MovieId) values (3,2), (5,2), (3,2), (3,2),(4,2), (5,2)",
                    "insert into dbo.Review values (7,2,'Muito Bom','Antonio','Muito Bom Mesmo'), " +
                            "(8,2,'Muito Bom','Ze','Tambem')",

                    "insert into dbo.Collections (name, description) " +
                            "values('Filmes recentes', 'Os melhores filmes recentes')",
                    "insert into dbo.Collections (name, description) " +
                            "values('Oscars', 'Oscars 2015')",
                    "insert into dbo.CollectionsMovie values (1, 1), (2,1), (1, 2)"
            }).execute();
        });
    }

    @AfterAll
    public static void cleanUp() throws SQLException {
        service.transaction((trans) ->
                trans.batch(new String[]{
                    "DELETE FROM CollectionsMovie",
                    "DELETE FROM Collections",
                    "DELETE FROM Review",
                    "DELETE FROM Rating",
                    "DELETE FROM Movie"
                }).execute()
        );
    }

    @Test
    public void test_connection() throws SQLException {
        service.query("select 1").execute();
    }

    @Test
    public void GetAllMoviesCommandTest() throws CommandException {
        Command<Page<Pair<Movie, Double>>> command= new GetAllMoviesCommand(service);
        List<Movie> expected = Arrays.asList(
                new Movie(1,"Django",2012, today),
                new Movie(2,"Shutter Island",2010, today)
        );
        List<Movie> result= command.execute(new CommandArguments()).getList().stream()
                .map(Pair::getFirst)
                .collect(toList());
        assertEquals(expected, result);
    }

    @Test
    public void GetCollectionCommandTest() throws CommandException {
        Command<MovieCollection> command= new GetCollectionCommand(service);

        MovieCollection expected = new MovieCollection(
                1, "Filmes recentes","Os melhores filmes recentes",
                Arrays.asList(new Movie(1,"Django",2012, today), new Movie(2,"Shutter Island",2010, today))
        );

        HashMap<String, String> vars = new HashMap<>();
        vars.put("{cid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(),new HashMap<>(),vars);

        MovieCollection result = command.execute(args);
        assertEquals(expected, result);
    }

    @Test
    public void DeleteMovieFromCollectionCommandTest() throws CommandException {
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1, "Django", 2012, today));
        MovieCollection expected = new MovieCollection(2, "Oscars", "Oscars 2015", movies);

        HashMap<String, String> vars = new HashMap<>();
        vars.put("{cid}","2");
        vars.put("{mid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(),new HashMap<>(),vars);

        Command<MovieCollection> getCommand = new GetCollectionCommand(service);

        MovieCollection result = getCommand.execute(args);
        assertEquals(expected, result);

        Command deleteCommand = new DeleteMovieFromCollectionCommand(service);
        deleteCommand.execute(args);

        getCommand = new GetCollectionCommand(service);
        result = getCommand.execute(args);

        assertTrue(result.getMovies().isEmpty());
        assertEquals(expected.getId(), result.getId());
        assertEquals(expected.getName(), result.getName());
        assertEquals(expected.getDescription(), result.getDescription());
    }

    @Test
    public void CollectionCommandsTest() throws CommandException {
        /* GET */
        Command<Page<MovieCollection>> getCommand = new GetAllCollectionsCommand(service);

        List<MovieCollection> expected = new ArrayList<>();
        expected.add(new MovieCollection(1, "Filmes recentes", "Os melhores filmes recentes"));
        expected.add(new MovieCollection(2, "Oscars", "Oscars 2015"));

        List<MovieCollection> getResult = getCommand.execute(new CommandArguments()).getList();
        assertEquals(expected, getResult);

        /* POST */
        Command<Long> postCommand= new PostCollectionCommand(service);

        HashMap<String, String> param = new HashMap<>();
        param.put("name", "Action");
        param.put("description", "Best Action Movies");
        CommandArguments args = new CommandArguments(new HashMap<>(), param, new HashMap<>());

        Long postResult = postCommand.execute(args);
        assertEquals(Long.valueOf(3), postResult);

        /* GET */
        expected.add(0, new MovieCollection(3, "Action", "Best Action Movies"));

        getResult = getCommand.execute(new CommandArguments()).getList();
        assertEquals(expected, getResult);
    }

    @Test
    public void GetMovieCommandTest() throws CommandException {
        Command<DetailedMovie> command = new GetMovieCommand(service);

        Movie expected = new Movie(1,"Django",2012, today);

        HashMap<String, String> vars = new HashMap<>();
        vars.put("{mid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        DetailedMovie result = command.execute(args);
        assertEquals(expected, result.getMovie());
    }

    @Test
    public void GetHigherAverageMovieCommandTest() throws CommandException {
        Command<Pair<Movie, Double>> command = new GetHigherAverageMovieCommand(service);
        Movie expected = new Movie(1, "Django", 2012, today);
        Movie result= command.execute(new CommandArguments()).getFirst();
        assertEquals(expected, result);
    }

    @Test
    public void GetHigherCountReviewsCommandTest() throws CommandException {
        Command<Pair<Movie, Long>> command = new GetHigherCountReviewCommand(service);
        Movie expected = new Movie(2, "Shutter Island", 2010, today);
        Movie result = command.execute(new CommandArguments()).getFirst();
        assertEquals(expected, result);
    }

    @Test
    public void GetLowestAverageMovieCommandTest() throws CommandException {
        Command<Pair<Movie, Double>> command = new GetLowestAverageMovieCommand(service);
        Movie expected = new Movie(2,"Shutter Island",2010, today);
        Movie result = command.execute(new CommandArguments()).getFirst();
        assertEquals(expected, result);
    }

    @Test
    public void GetRatingCommandTest() throws CommandException {
        Command<RatingInformation> command = new GetRatingCommand(service);

        RatingInformation expected = new RatingInformation(1, 25D/6, 0, 0, 1, 3, 2);

        HashMap<String,String> vars = new HashMap<>();
        vars.put("{mid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        RatingInformation result = command.execute(args);
        assertEquals(expected, result);
    }

    @Test
    public void GetReviewsFromMovieCommandTest() throws CommandException {
        GetReviewsFromMovieCommand command = new GetReviewsFromMovieCommand(service);

        List<Review> expected = Collections.singletonList(new Review(1, 1, 4, "Antonio", "Muito Bonito"));

        HashMap<String,String> vars = new HashMap<>();
        vars.put("{mid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        List<Review> result = command.execute(args).getSecond().getList();
        assertEquals(expected, result);
    }

    @Test
    public void GetSpecificReviewFromMovieCommandTest() throws CommandException {
        Command<Review> command = new GetSpecificReviewFromMovieCommand(service);

        Review expected = new Review(1, 1, 4, "Antonio", "Muito Bonito", "Muito Bonito Mesmo");

        HashMap<String,String> vars = new HashMap<>();
        vars.put("{mid}","1");
        vars.put("{rid}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        Review result = command.execute(args);
        assertEquals(expected, result);
    }

    @Test
    public void GetTopHigherAverageMoviesCommandTest() throws CommandException {
        Command<List<Pair<Movie, Double>>> command = new GetTopHigherAverageMoviesCommand(service);

        List<Movie> expected = Arrays.asList(
            new Movie(1, "Django", 2012, today),
            new Movie(2, "Shutter Island", 2010, today)
        );

        HashMap<String,String> vars = new HashMap<>();
        vars.put("{n}","2");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        List<Movie> result = command.execute(args).stream()
                .map(Pair::getFirst)
                .collect(toList());
        assertEquals(expected, result);
    }

    @Test
    public void GetTopHigherCountReviewsCommandTest() throws CommandException {
        GetTopHigherCountReviewsCommand command = new GetTopHigherCountReviewsCommand(service);

        List<Movie> expected=new ArrayList<>();
        expected.add(new Movie(2,"Shutter Island", 2010, today));

        HashMap<String,String> vars = new HashMap<>();
        vars.put("{n}","1");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        List<Movie> result = command.execute(args).stream()
                .map(Pair::getFirst)
                .collect(toList());
        assertEquals(expected, result);
    }

    @Test
    public void GetTopLowestAverageMovieCommandTest() throws CommandException {
        Command<List<Pair<Movie, Double>>> command = new GetTopLowestAverageMovieCommand(service);

        List<Movie> expected = Arrays.asList(
                new Movie(2, "Shutter Island", 2010, today),
                new Movie(1, "Django", 2012, today)
        );

        HashMap<String,String> vars=new HashMap<>();
        vars.put("{n}","2");
        CommandArguments args = new CommandArguments(new HashMap<>(), new HashMap<>(), vars);

        List<Movie> result = command.execute(args).stream()
                .map(Pair::getFirst)
                .collect(toList());
        assertEquals(expected, result);
    }
}
