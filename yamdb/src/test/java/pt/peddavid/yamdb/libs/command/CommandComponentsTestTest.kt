package pt.peddavid.yamdb.libs.command

import org.eclipse.jetty.http.HttpMethod
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import pt.peddavid.commons.Match
import pt.peddavid.commons.NoMatch

class CommandComponentsTestTest {
    @Test
    fun `GET is a method`() {
        val methodResult = MethodMatcher.tryMatch("GET")
        Assert.assertEquals(HttpMethod.GET, (methodResult as Match<HttpMethod>).result)
    }

    @Test
    fun `DUMMY_TEST is a method`() {
        val methodGet = "DUMMY_TEST"
        val methodResult = MethodMatcher.tryMatch(methodGet)
        Assertions.assertTrue(methodResult is NoMatch)
    }

    @Test
    fun `Path must not be empty and start with slash`() {
        val emptyPathResult = PathMatcher.tryMatch("")
        val startWithoutSlashPathResult = PathMatcher.tryMatch("test")

        val startsWithSlashPathResult = PathMatcher.tryMatch("/")

        Assertions.assertTrue(emptyPathResult is NoMatch)
        Assertions.assertTrue(startWithoutSlashPathResult is NoMatch)

        Assertions.assertEquals("/", (startsWithSlashPathResult as Match<String>).result)
    }

    @Test
    fun `headers are correctly deduced`() {
        val emptyHeadersResult = HeadersMatcher.tryMatch("")
        val noKeyValueResult = HeadersMatcher.tryMatch("something|test")

        val singleKeyValueResult = HeadersMatcher.tryMatch("something:test")
        val multipleKeyValueResult = HeadersMatcher.tryMatch("something:test|test:somewhat")

        val oneKeyValueWithoutValueResult = HeadersMatcher.tryMatch("something:what|test")

        Assertions.assertTrue(emptyHeadersResult is NoMatch)
        Assertions.assertTrue(noKeyValueResult is NoMatch)

        Assertions.assertEquals(mapOf("something" to "test"), (singleKeyValueResult as Match<Map<String, String>>).result)
        Assertions.assertEquals(mapOf("something" to "test", "test" to "somewhat"), (multipleKeyValueResult as Match<Map<String, String>>).result)

        Assertions.assertTrue(oneKeyValueWithoutValueResult is NoMatch)
    }

    @Test
    fun `arguments are correctly deduced`() {
        val emptyParametersResult = ParametersMatcher.tryMatch("")
        val noKeyValueResult = ParametersMatcher.tryMatch("something&test")

        val singleKeyValueResult = ParametersMatcher.tryMatch("something=test")
        val multipleKeyValueResult = ParametersMatcher.tryMatch("something=test&test=somewhat")

        val oneKeyValueWithoutValueResult = ParametersMatcher.tryMatch("something=what&test")

        Assertions.assertTrue(emptyParametersResult is NoMatch)
        Assertions.assertTrue(noKeyValueResult is NoMatch)

        Assertions.assertEquals(mapOf("something" to "test"), (singleKeyValueResult as Match<Map<String, String>>).result)
        Assertions.assertEquals(mapOf("something" to "test", "test" to "somewhat"), (multipleKeyValueResult as Match<Map<String, String>>).result)

        Assertions.assertTrue(oneKeyValueWithoutValueResult is NoMatch)
    }
}
