package pt.peddavid.yamdb;

import kotlin.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.CommandManager;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class CommandManagerTest {

    private static CommandManager mgr = new CommandManager(path -> path.split(" "));
    private static Command emptyCommand = (args) -> null;

    @BeforeAll
    public static void setUp() {
        mgr.addCommand(
                "GET /path/is/empty", (args) -> {
                    assertTrue(args.isEmpty());
                    return null;
                }
        );
        mgr.addCommand(
                "GET /path/is/not/{empty}", (args) -> {
                    assertTrue(!args.isEmpty());
                    return null;
                }
        );
    }

    /* Invalid CommandStrings */
    @Test
    public void addInvalidCommands(){
        assertFalse(mgr.addCommand("Get", emptyCommand));
        assertFalse(mgr.addCommand("GET add", emptyCommand));
    }

    @Test
    public void emptyCommand() {
        assertThrows(CommandException.class, () -> mgr.getCommand(""));
    }

    @Test
    public void lowerCaseMethod() {
        assertThrows(CommandException.class, () -> mgr.getCommand("Got"));
    }

    @Test
    public void pathWithoutEntryBar() {
        assertThrows(CommandException.class, () -> mgr.getCommand("GOT something"));
    }

    @Test
    public void headerAndArgumentsReverseOrder() {
        assertThrows(CommandException.class, () -> mgr.getCommand("GOT /something key=value pair:empty"));
    }

    @Test
    public void emptyValues() {
        assertThrows(CommandException.class, () -> mgr.getCommand("GOT /something key:value pair="));
    }

    /* VALID CMD Commands */

    @Test
    public void MethodAndPath() throws CommandException {
        Pair<Command, CommandArguments> cmd = mgr.getCommand("GET /path/is/empty");
        assertNotNull(cmd);
        cmd.getFirst().execute(cmd.getSecond());
        assertTrue(cmd.getSecond().isEmpty());
    }

    @Test
    public void AllFields() throws CommandException {
        Pair<Command, CommandArguments> cmd = mgr.getCommand("GET /path/is/not/empty key:value pair=notEmpty");
        assertNotNull(cmd);
        cmd.getFirst().execute(cmd.getSecond());
        cmd.getSecond().optionalHeader("key")
                .ifPresent((value) -> assertEquals("value", value));
        cmd.getSecond().optionalParameter("pair")
                .ifPresent((value) -> assertEquals("notEmpty", value));
    }

    //TODO:
    @Test
    @Disabled("Fix implementation asap")
    public void routerAllowsMultipleVariablesInSamePlace() throws CommandException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}", args -> args);
        mgr.addCommand("GET /path/{y}/info", args -> args);
        Pair<Command, CommandArguments> command = mgr.getCommand("GET /path/3/info");
        Pair<Command, CommandArguments> command2 = mgr.getCommand("GET /path/3");
        assertNotNull(command);
        assertNotNull(command2);
    }

    //TODO:
    @Test
    @Disabled("Fix implementation asap")
    public void routerVariablesDoNotLeakFromOtherRoutes() throws CommandException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}", args -> args);
        mgr.addCommand("GET /path/{y}/info", args -> args);
        Pair<Command, CommandArguments> command = mgr.getCommand("GET /path/3/info");
        Pair<Command, CommandArguments> command2 = mgr.getCommand("GET /path/3");
        assertNotNull(command);
        assertNotNull(command2);
    }

    @Test
    @Disabled("Needs a way to assert minimal number of nodes were created")
    public void testOptimizedStorage() throws CommandException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}/test", args -> args);
        mgr.addCommand("GET /path/{y}/info", args -> args);

        mgr.getCommand("GET /path/123/info");
    }

    @Test
    public void testDifferentNodesWereCreatedForEachVariable() throws CommandException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}/test", args -> args);
        mgr.addCommand("GET /path/{y}/info", args -> args);

        Pair<Command, CommandArguments> command = mgr.getCommand("GET /path/123/info");
        int i = command.getSecond().demandIntVariable("{y}");
    }

    @Test
    public void searches() throws InterruptedException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}/variables/{y}", args -> args);
        mgr.addCommand("POST /path/{x}/variables/{y}", args -> args);

        for(int i = 0; i < 40; ++i) {
            try {
                Pair<Command, CommandArguments> command = mgr.getCommand((i % 2 == 0 ? "GET" : "POST") + " /path/" + i + "/variables/" + (i + 1));
                assertEquals(i, command.getSecond().demandIntVariable("{x}"));
                assertEquals(i + 1, command.getSecond().demandIntVariable("{y}"));
            } catch (CommandException e) {
                fail("Context clears set component to null making command route invalid");
                System.err.print(e);
            }
        }
    }

    @Test
    public void concurrentSearches() throws InterruptedException {
        final CommandManager mgr = new CommandManager(path -> path.split(" "));
        mgr.addCommand("GET /path/{x}/variables/{y}", args -> args);
        mgr.addCommand("POST /path/{x}/variables/{y}", args -> args);

        List<Callable<Result>> collect = IntStream.range(0, 50)
                .mapToObj(x -> (Callable<Result>) () -> {
                    final String method = x % 2 == 0 ? "GET" : "POST";
                    final int y = x + 1;
                    Pair<Command, CommandArguments> command = mgr.getCommand(method + " /path/" + x + "/variables/" + y);
                    CommandArguments args = command.getSecond();

                    return new Result(x, args.demandIntVariable("{x}"),
                            y, args.demandIntVariable("{y}"));
                })
                .collect(Collectors.toList());

        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future<Result>> futures = executorService.invokeAll(collect);
        futures.forEach(future -> {
            assertTrue(future.isDone());
            try {
                Result res = future.get();
                assertEquals(res.expectedX, res.actualX);
                assertEquals(res.expectedY, res.actualY);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static class Result {
        final int expectedX;
        final int actualX;
        final int expectedY;
        final int actualY;

        private Result(int expectedX, int actualX, int expectedY, int actualY) {
            this.expectedX = expectedX;
            this.actualX = actualX;
            this.expectedY = expectedY;
            this.actualY = actualY;
        }
    }
}
