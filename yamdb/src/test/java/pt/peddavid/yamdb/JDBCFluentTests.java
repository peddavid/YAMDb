package pt.peddavid.yamdb;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled("Ignore @BeforeClass")
public class JDBCFluentTests {

    private static ConnectionService service;

    @BeforeAll
    public static void setUp() throws SQLException {
        service = new MSSQLService();
    }

    @Test
    @Disabled("Doesn't assert anything")
    public void test() throws IOException, SQLException {
        service.query("select id, title, releaseYear, addedDate from Movie")
                .map((rs) -> new Movie(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4).toLocalDate()))
                .sort("id desc, title")
                .top(4)
                .forEach(System.out::println);
    }

    @Test
    @Disabled("Invalid precondition: Table Movie exists")
    public void collectTest() throws SQLException {
        List<Movie> movies = service.query("select id, title, releaseYear, addedDate from Movie")
                .map((rs) -> new Movie(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4).toLocalDate()))
                .sort("id desc, title")
                .collect(new Collector<Movie, List<Movie>, List<Movie>>() {
                    @Override
                    public Supplier<List<Movie>> supplier() {
                        return ArrayList::new;
                    }

                    @Override
                    public BiConsumer<List<Movie>, Movie> accumulator() {
                        return (l, m) -> {
                            if(m.getId() != 6){
                                l.add(m);
                            }
                        };
                    }

                    @Override
                    public BinaryOperator<List<Movie>> combiner() {
                        return (l1, l2) -> {
                            l1.addAll(l2);
                            return l1;
                        };
                    }

                    @Override
                    public Function<List<Movie>, List<Movie>> finisher() {
                        return Function.identity();
                    }

                    @Override
                    public Set<Characteristics> characteristics() {
                        return null;
                    }
                });
        for(Movie movie : movies){
            assertTrue(movie.getId() != 6);
        }
    }
}
