package pt.peddavid.yamdb.model;

import java.util.ArrayList;
import java.util.List;

public class MovieCollection {

    private final int id;
    private final String name;
    private final String description;

    private final List<Movie> movies;

    public MovieCollection(int id, String name, String description, List<Movie> movies) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.movies = movies;
    }

    public MovieCollection(int id, String name, String description) {
        this(id, name, description, new ArrayList<>());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieCollection that = (MovieCollection) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return !(movies != null ? !movies.equals(that.movies) : that.movies != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (movies != null ? movies.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("id = ").append(id)
                .append(", name = ").append(name);

        if(!description.isEmpty()){
            builder.append(", description = ").append(description);
        }

        if (!movies.isEmpty()) {
            builder.append("\n Movies: ");
            for (Movie movie : movies)
                builder.append("\n").append(movie.toString());
        }
        return builder.toString();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Movie> getMovies() {
        return movies;
    }
}
