package pt.peddavid.yamdb.model;

public class Review extends Rating {

    private final String reviewSummary;
    private final String fullReview;
    private final String author;

    public Review(int id, int movieId, int score, String author, String reviewSummary) {
        this(id, movieId, score, author, reviewSummary, "");
    }

    public Review(int id, int movieId, int score, String author, String reviewSummary, String fullReview) {
        super(id,movieId,score);
        this.reviewSummary = reviewSummary;
        this.fullReview = fullReview;
        this.author=author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Review review = (Review) o;

        if (!reviewSummary.equals(review.reviewSummary)) return false;
        if (!fullReview.equals(review.fullReview)) return false;
        return author.equals(review.author);

    }

    @Override
    public int hashCode() {
        int result = reviewSummary.hashCode();
        result = 31 * result + fullReview.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String str= super.toString()  +
                " ReviewSummary='" + reviewSummary + "\'";
        if(fullReview!=null)
            str+="FullReview='" + fullReview +"\'";
        return str;
    }

    public String getReviewSummary() {
        return reviewSummary;
    }

    public String getFullReview() {
        return fullReview;
    }

    public String getAuthor() {
        return author;
    }
}
