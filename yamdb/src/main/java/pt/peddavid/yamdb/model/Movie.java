package pt.peddavid.yamdb.model;

import java.sql.Date;
import java.time.LocalDate;

public class Movie {

    private final int id;
    private final int releaseYear;
    private final String title;
    private final LocalDate addedDate;

    public Movie(int id, String title, int releaseYear, LocalDate addedDate) {
        this.id = id;
        this.title = title;
        this.releaseYear = releaseYear;
        this.addedDate = addedDate;
    }

    public Movie(int id, String title, int releaseYear, Date addedDate) {
        this(id, title, releaseYear, addedDate != null ? addedDate.toLocalDate() : null);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (id != movie.id) return false;
        if (releaseYear != movie.releaseYear) return false;
        if (!title.equals(movie.title)) return false;
        return addedDate.equals(movie.addedDate);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + releaseYear;
        result = 31 * result + title.hashCode();
        result = 31 * result + addedDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return  "{ id = " + id +
                ", title = " + title +
                ", releaseYear = " + releaseYear +
                ", addedDate = " + addedDate + " }";
    }
}
