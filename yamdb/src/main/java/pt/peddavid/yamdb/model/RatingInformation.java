package pt.peddavid.yamdb.model;

public class RatingInformation {

    private final int movieId;
    private final double average;
    private final int oneStar;
    private final int twoStar;
    private final int threeStar;
    private final int fourStar;
    private final int fiveStar;

    public RatingInformation(
            int movieId,
            double average,
            int oneStar,
            int twoStar,
            int threeStar,
            int fourStar,
            int fiveStar
    ){
        this.movieId = movieId;
        this.average = average;
        this.oneStar = oneStar;
        this.twoStar = twoStar;
        this.threeStar = threeStar;
        this.fourStar = fourStar;
        this.fiveStar = fiveStar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RatingInformation that = (RatingInformation) o;

        if (movieId != that.movieId) return false;
        if (Double.compare(that.average, average) != 0) return false;
        if (oneStar != that.oneStar) return false;
        if (twoStar != that.twoStar) return false;
        if (threeStar != that.threeStar) return false;
        if (fourStar != that.fourStar) return false;
        return fiveStar == that.fiveStar;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = movieId;
        temp = Double.doubleToLongBits(average);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + oneStar;
        result = 31 * result + twoStar;
        result = 31 * result + threeStar;
        result = 31 * result + fourStar;
        result = 31 * result + fiveStar;
        return result;
    }

    @Override
    public String toString() {
        return  "MovieId=" + movieId +
                ", Average=" + average +
                ", 1 Star=" + oneStar +
                ", 2 Stars=" + twoStar +
                ", 3 Stars=" + threeStar +
                ", 4 Stars=" + fourStar +
                ", 5 Stars=" + fiveStar;
    }

    public int getMovieId() {
        return movieId;
    }

    public double getAverage() {
        return average;
    }

    public int getOneStar() {
        return oneStar;
    }

    public int getTwoStar() {
        return twoStar;
    }

    public int getThreeStar() {
        return threeStar;
    }

    public int getFourStar() {
        return fourStar;
    }

    public int getFiveStar() {
        return fiveStar;
    }
}
