package pt.peddavid.yamdb.model;

public class Rating {

    protected final int id;
    protected final int movieId;
    protected final int score;

    public Rating(int id, int movieId, int score) {
        this.id = id;
        this.movieId = movieId;
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (id != rating.id) return false;
        if (movieId != rating.movieId) return false;
        return score == rating.score;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + movieId;
        result = 31 * result + score;
        return result;
    }

    @Override
    public String toString() {
        return  "Id=" + id +
                ", MovieId=" + movieId +
                ", Score=" + score;
    }

    public int getId() {
        return id;
    }

    public int getMovieId() {
        return movieId;
    }

    public int getScore() {
        return score;
    }
}
