package pt.peddavid.yamdb.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * A Page is just a conceptual representation of a List<p>
 *  The skip doesn't "truly skip" anything, just tells if this Page
 *      represents something that can access previous data<p>
 *  The top parameter represents the effective size of this Page,
 *      (getList() will only return a subList with size() == Top)<p>
 *  Both parameters are represented as optional.<p>
 *  The recommended use case of Page is providing the wanted top
 *      and supplying a bigger a possibly bigger List, so hasNext() is (top < list.size())<p>
 *  Despite this, the skip and top are accessible with getters and the user may choose
 *      another criteria to calculate "hasPrevious()" and "hasNext()"
 * @param <E> the type of elements in the page
 */
public class Page<E> {

    private static final Logger log = LoggerFactory.getLogger(Page.class);

    private final List<E> list;
    private final Optional<Integer> skip, top;

    public Page(Optional<Integer> skip, Optional<Integer> top){
        this(skip, top, new ArrayList<>());
    }

    public Page(Optional<Integer> skip, Optional<Integer> top, List<E> list){
        if(skip == null || top == null || list == null) {
            throw new IllegalArgumentException("skip=" + Objects.toString(skip, "null") +
                    ",top=" + Objects.toString(top, "null") +
                    ",list=" + Objects.toString(list, "null"));
        } else if(skip.isPresent() && skip.get() < 0) {
            throw new IllegalArgumentException("skip < 0");
        } else if(top.isPresent() && top.get() < 0) {
            throw new IllegalArgumentException("top < 0");
        }

        this.skip = skip;
        this.top = top;
        this.list = list;
    }

    public Page<E> add(E item){
        list.add(item);
        return this;
    }

    public List<E> getList(){
        return top
                .map((topValue) -> list.subList(0, topValue > list.size() ?
                        list.size() :
                        topValue))
                .orElse(list);
    }

    public Stream<E> stream(){
        return getList().stream();
    }

    public boolean hasPrevious(){
        return skip.isPresent() && skip.get() > 0;
    }

    public boolean hasNext(){
        return top.isPresent() && top.get() < list.size();
    }

    public Optional<Integer> getSkip() {
        return skip;
    }

    public Optional<Integer> getTop() {
        return top;
    }

    public String getQuery() {
        StringBuilder builder = new StringBuilder();
        skip.ifPresent((value) -> builder
                .append("skip=")
                .append(value));
        top.ifPresent((value) -> builder
                .append(skip.isPresent() ? "&top=" : "top=")
                .append(value));
        return builder.toString();
    }

    public String getPreviousQuery() {
        StringBuilder builder = new StringBuilder();
        if(hasPrevious()){
            int origin = skip.get() - top.orElse(Integer.MAX_VALUE);
            origin = origin < 0 ? 0 : origin;
            builder.append("skip=").append(origin);
            top.ifPresent(topValue -> builder.append("&top=").append(topValue));
        }
        return builder.toString();
    }

    public String getNextQuery() {
        return hasNext() ? "skip=" + (skip.orElse(0) + top.get())
                + "&top=" + top.get()
                : "";
    }

    @Override
    public String toString(){
        return "Page:" + list.toString() +
                (hasPrevious() ? " <Has Previous>" : "") +
                (hasNext() ? " <Has Next>" : "");
    }

    public static<E> Collector<E, Page<E>, Page<E>> getCollector(
            Optional<Integer> skip,
            Optional<Integer> top
    ){
        return new Collector<E, Page<E>, Page<E>>() {
            @Override
            public Supplier<Page<E>> supplier() {
                return () -> new Page<>(skip, top);
            }

            @Override
            public BiConsumer<Page<E>, E> accumulator() {
                return Page::add;
            }

            @Override
            public BinaryOperator<Page<E>> combiner() {
                return null;
            }

            @Override
            public Function<Page<E>, Page<E>> finisher() {
                return Function.identity();
            }

            @Override
            public Set<Characteristics> characteristics() {
                return null;
            }
        };
    }
}
