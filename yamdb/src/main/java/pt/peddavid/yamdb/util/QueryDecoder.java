package pt.peddavid.yamdb.util;

import pt.peddavid.yamdb.libs.command.CommandComponent;
import pt.peddavid.yamdb.libs.command.CommandComponents;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class QueryDecoder {

    public static String decode(HttpServletRequest req) throws IOException {
        String referer = req.getHeader("referer");
        String[] save = req.getParameterValues("save");
        if(save != null && referer != null){
            String[] saves = save[0].split(",");

            URL requestUrl = new URL(referer);
            Map<String, String> refererParameterMap = new HashMap<>();
            CommandComponent parameterSolver = CommandComponents
                    .parametersComponentCaptureTo(refererParameterMap);
            parameterSolver.test(requestUrl.getQuery());

            String additionalQuery = refererParameterMap.entrySet().stream()
                    .filter((entry) -> !req.getParameterMap().containsKey(entry.getKey()))
                    .filter((entry) -> Stream.of(saves)
                            .anyMatch((item) -> item.equals(entry.getKey())))
                    .map((entry) -> entry.getKey() + "=" + entry.getValue())
                    .reduce((x, y) -> x + "&" + y)
                    .orElse("");

            String prevQuery = req.getParameterMap().entrySet().stream()
                    .filter((entry) -> !entry.getKey().equals("save"))
                    .map((entry) -> entry.getKey() + "=" + entry.getValue()[0])
                    .reduce((x, y) -> x + "&" + y)
                    .orElse("");

            return "?" +    (!prevQuery.isEmpty() ? prevQuery : "") +
                            (!prevQuery.isEmpty() && !additionalQuery.isEmpty() ? "&" : "") +
                            (!additionalQuery.isEmpty() ? additionalQuery : "");
        }
        return req.getQueryString();
    }
}
