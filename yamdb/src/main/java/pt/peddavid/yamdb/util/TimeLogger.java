package pt.peddavid.yamdb.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class TimeLogger {

    public interface SupplierWithThrowException<E, Ex extends Throwable> {
        E get() throws Ex;
    }

    public static <E, Ex extends Throwable> E logTimeWithThrow(
            SupplierWithThrowException<E, Ex> function,
            Class<?> type,
            String message
    ) throws Ex {
        Logger log = LoggerFactory.getLogger(type);
        long startTime = System.nanoTime();
        E result = function.get();
        long interval = System.nanoTime() - startTime;
        log.info("{} -> {}", message, decode(interval));
        return result;
    }

    public static <E> E logTimeAndSupply(
            Supplier<E> function,
            Class<?> type, String message
    ) {
        return logTimeAndSupply(function, LoggerFactory.getLogger(type), message);
    }

    public static <E>E logTimeAndSupply(Supplier<E> function, Logger log, String message) {
        long startTime = System.nanoTime();
        E result = function.get();
        long interval = System.nanoTime() - startTime;
        log.info("{} -> {}", message,  decode(interval));
        return result;
    }

    public static void logTime(Runnable function, Logger log, String message) {
        long startTime = System.nanoTime();
        function.run();
        long interval = System.nanoTime() - startTime;
        log.info("{} -> {}", message,  decode(interval));
    }

    private static String decode(long interval){
        String precision = "ns";
        if(interval > TimeUnit.SECONDS.toNanos(10)) {
            interval = TimeUnit.NANOSECONDS.toSeconds(interval);
            precision = "s";
        } else if(interval > TimeUnit.MILLISECONDS.toNanos(10)) {
            interval = TimeUnit.NANOSECONDS.toMillis(interval);
            precision = "ms";
        } else if (interval > TimeUnit.NANOSECONDS.toNanos(10)) {
            interval = TimeUnit.NANOSECONDS.toMicros(interval);
            precision = "us";
        }
        return interval + precision;
    }
}
