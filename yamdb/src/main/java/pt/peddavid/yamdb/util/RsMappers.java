package pt.peddavid.yamdb.util;

import kotlin.Pair;
import pt.peddavid.yamdb.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;


/** This class works as a "Functional Factory", easing the job of Model objects composition
 * and avoiding some code replication
 */
public class RsMappers {

    private RsMappers() { }


    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4)}
     * @return new Movie(id, title, releaseYear, AddedDate)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Movie Movie(ResultSet rs) throws SQLException {
        return new Movie(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4), rs.getDouble(5) }
     * @return new Pair(new Movie(id, title, releaseYear, AddedDate), average)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Pair<Movie, Double> MovieWithAverage(ResultSet rs) throws SQLException {
        return new Pair<>(Movie(rs), rs.getDouble(5));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4), rs.getLong(5) }
     * @return new Pair(new Movie(id, title, releaseYear, AddedDate), count)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Pair<Movie, Long> MovieWithCount(ResultSet rs) throws SQLException {
        return new Pair<>(Movie(rs), rs.getLong(5));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5) }
     * @return return new Review(id, movieId, score, author, summary)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Review Review(ResultSet rs) throws SQLException {
        return new Review(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6) }
     * @return return new Review(id, movieId, score, author, summary, fullReview)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Review FullReview(ResultSet rs) throws SQLException {
        return new Review(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6));
    }

    /**
     * @param rs has to be compatible with { rs rs.getInt(1), rs.getString(2), rs.getString(3) }
     * @return new MovieCollection(id, name, description)
     * @throws SQLException if rs is not compatible with signature
     */
    public static MovieCollection Collection(ResultSet rs) throws SQLException {
        return new MovieCollection(rs.getInt(1), rs.getString(2), rs.getString(3));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getInt(2), rs.getInt(3) }
     * @return new Rating(id, movieId, score)
     * @throws SQLException if rs is not compatible with signature
     */
    public static Rating Rating(ResultSet rs) throws SQLException {
        return new Rating(rs.getInt(1), rs.getInt(2),  rs.getInt(3));
    }

    /**
     * @param rs has to be compatible with { rs.getInt(1), rs.getDouble(2), rs.getInt(3), rs.getInt(4),
     *           rs.getInt(5), rs.getInt(6), rs.getInt(7) }
     * @return new RatingInformation(movieId, average, oneStar, twoStar, threeStar, fourStar, fiveStar)
     * @throws SQLException if rs is not compatible with signature
     */
    public static RatingInformation RatingInformation(ResultSet rs) throws SQLException{
        return new RatingInformation(
                rs.getInt(1),
                rs.getDouble(2),
                rs.getInt(3),
                rs.getInt(4),
                rs.getInt(5),
                rs.getInt(6),
                rs.getInt(7));
    }
}
