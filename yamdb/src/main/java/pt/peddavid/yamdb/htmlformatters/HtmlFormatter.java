package pt.peddavid.yamdb.htmlformatters;

import pt.peddavid.commons.Formatter;
import pt.peddavid.html.HtmlElement;

@FunctionalInterface
public interface HtmlFormatter<E> extends Formatter<E> {
    @Override
    default String format(E source){
        return supplyHtml(source).toHtmlDocument();
    }
    HtmlElement supplyHtml(E source);
}
