package pt.peddavid.yamdb.htmlformatters;

import kotlin.Pair;
import pt.peddavid.yamdb.htmlformatters.util.HtmlForm;
import pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink;
import pt.peddavid.yamdb.htmlformatters.util.HtmlTable;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.model.MovieCollection;
import pt.peddavid.yamdb.util.Page;

import static pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink.hyperLink;

public class HtmlCollectionFormatter {

    private static final HtmlTable<MovieCollection> TABLE = getTable();

    public static class SimpleCollectionFormatter implements HtmlFormatter<MovieCollection> {

        private static final HtmlTable<Movie> MOVIE_TABLE = HtmlMovieFormatter.getTable();
        private static final HtmlForm FORM = new HtmlForm("Post Movie To Collection",
                new Pair<>("Movie Id", "mid"));

        @Override
        public HtmlElement supplyHtml(MovieCollection source) {
            return HtmlFactory.body().with(
                    HtmlFactory.h3(String.valueOf(source.getId()) + ". " + source.getName()),
                    HtmlFactory.p().with(HtmlFactory.b("Description: "), HtmlFactory.text(source.getDescription())),
                    MOVIE_TABLE.supplyHtmlFromList(source.getMovies()),
                    FORM.action("../" + source.getId() + "/"),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Back", "../")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static class CollectionPageFormatter implements HtmlFormatter<Page<MovieCollection>> {

        private static final HtmlForm FORM = new HtmlForm("Post Collection",
                new Pair<>("Name", "name"),
                new Pair<>("Description", "description"))
                .action("../collections/");

        @Override
        public HtmlElement supplyHtml(Page<MovieCollection> source) {
            return HtmlFactory.body().with(
                    TABLE.supplyHtmlFromPage(source),
                    FORM,
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static HtmlTable<MovieCollection> getTable(){
        return new HtmlTable<MovieCollection>()
                .setTitle("Collections")
                .setHeaders("ID", "Name", "Description")
                .addHtml((source) -> hyperLink(source.getId(),
                        "/collections/" + source.getId() + "/"))
                .addString(MovieCollection::getName)
                .addString(MovieCollection::getDescription);
    }
}
