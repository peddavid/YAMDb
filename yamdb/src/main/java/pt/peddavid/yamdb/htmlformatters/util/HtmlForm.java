package pt.peddavid.yamdb.htmlformatters.util;

import kotlin.Pair;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.html.HtmlNestedElement;

/** Only works with "Post forms" because we didn't use for anything else*/
public class HtmlForm extends HtmlNestedElement {

    @SafeVarargs
    public HtmlForm(String title, Pair<String, String>... fields) {
        super("form");

        HtmlNestedElement fieldSet = HtmlFactory.fieldSet().with(HtmlFactory.legend(title));
        for(Pair<String, String> field : fields){
            fieldSet.with(
                    HtmlFactory.text(field.getFirst()),
                    HtmlFactory.br(),
                    HtmlFactory.input().setAttribute("type", "text")
                            .setAttribute("name", field.getSecond()),
                    HtmlFactory.br()
            );
        }
        fieldSet.with(
                HtmlFactory.br(),
                HtmlFactory.input().setAttribute("type", "submit")
                        .setAttribute("value", "Submit")
        );

        this.with(
                fieldSet
        ).setAttribute("method", "post");
    }

    public HtmlForm action(String link){
        this.setAttribute("action", link);
        return this;
    }
}
