package pt.peddavid.yamdb.htmlformatters;

import kotlin.Pair;
import pt.peddavid.yamdb.htmlformatters.util.HtmlForm;
import pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink;
import pt.peddavid.yamdb.htmlformatters.util.HtmlTable;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.yamdb.model.Review;
import pt.peddavid.yamdb.util.Page;

import static pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink.hyperLink;

public class HtmlReviewFormatter {

    private static final HtmlTable<Review> TABLE = getTable();

    public static class SingleReviewFormatter implements HtmlFormatter<Review> {
        @Override
        public HtmlElement supplyHtml(Review source) {
            int movieId = source.getMovieId();
            return HtmlFactory.body().with(
                    HtmlFactory.p().with(HtmlFactory.b("Movie ID: "), HtmlFactory.text(Integer.toString(movieId))),
                    HtmlFactory.p().with(HtmlFactory.b("Rating ID: "), HtmlFactory.text(Integer.toString(source.getId()))),
                    HtmlFactory.p().with(HtmlFactory.b("Score: "), HtmlFactory.text(Integer.toString(source.getScore()))),
                    HtmlFactory.p().with(HtmlFactory.b("Author: "), HtmlFactory.text(source.getAuthor())),
                    HtmlFactory.p().with(HtmlFactory.b("Summary: "), HtmlFactory.text(source.getReviewSummary())),
                    HtmlFactory.p().with(HtmlFactory.b("Full Review: "), HtmlFactory.text(source.getFullReview())),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Reviews of Movie " + movieId, "../")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static class ReviewPageFormatter implements HtmlFormatter<Pair<Integer, Page<Review>>> {

        private static final HtmlForm FORM = new HtmlForm("Post Review",
                new Pair<>("Reviewer Name","reviewerName"),
                new Pair<>("Review Summary","reviewSummary"),
                new Pair<>("Complete Review","review"),
                new Pair<>("Final Rating", "rating"))
                .action("../reviews/");

        @Override
        public HtmlElement supplyHtml(Pair<Integer, Page<Review>> source) {
            return HtmlFactory.body().with(
                    TABLE.setTitle("Reviews of Movie " + source.getFirst())
                            .supplyHtmlFromPage(source.getSecond()),
                    FORM,
                    HtmlFactory.h3().with(hyperLink("Movie " + source.getFirst(), "../")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static HtmlTable<Review> getTable(){
        return new HtmlTable<Review>()
                .setHeaders("Review ID", "Score", "Author", "Summary")
                .addHtml((source) -> hyperLink(source.getId(),
                        "/movies/" + source.getMovieId() + "/reviews/" + source.getId() + "/"))
                .addInt(Review::getScore)
                .addString(Review::getAuthor)
                .addString(Review::getReviewSummary);
    }
}
