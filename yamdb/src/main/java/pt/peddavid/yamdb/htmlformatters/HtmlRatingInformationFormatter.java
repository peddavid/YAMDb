package pt.peddavid.yamdb.htmlformatters;

import kotlin.Pair;
import pt.peddavid.yamdb.htmlformatters.util.HtmlForm;
import pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink;
import pt.peddavid.yamdb.htmlformatters.util.HtmlTable;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.yamdb.model.RatingInformation;

import java.text.DecimalFormat;

public class HtmlRatingInformationFormatter implements HtmlFormatter<RatingInformation> {

    private static final DecimalFormat DOUBLE_FORMATTER = new DecimalFormat("#.##");

    private static final HtmlTable<RatingInformation> TABLE = getTable();

    private static final HtmlForm FORM =
            new HtmlForm("Post Rating", new Pair<>("Rating", "rating"))
            .action("../ratings/");

    @Override
    public HtmlElement supplyHtml(RatingInformation source) {
        int movieId = source.getMovieId();
        return HtmlFactory.body().with(
                TABLE.setTitle("Rating Information of Movie " + movieId)
                        .supplyHtml(source),
                FORM,
                HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Movie " + movieId, "../")),
                HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
        );
    }

    public static HtmlTable<RatingInformation> getTable() {
        return new HtmlTable<RatingInformation>()
                .setHeaders("1 Star", "2 Star", "3 Star", "4 Star", "5 Star", "Average")
                .addInt(RatingInformation::getOneStar)
                .addInt(RatingInformation::getTwoStar)
                .addInt(RatingInformation::getThreeStar)
                .addInt(RatingInformation::getFourStar)
                .addInt(RatingInformation::getFiveStar)
                .addString((source) -> DOUBLE_FORMATTER.format(source.getAverage()));
    }

    public static DecimalFormat getDoubleFormatter(){
        return DOUBLE_FORMATTER;
    }
}
