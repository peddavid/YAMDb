package pt.peddavid.yamdb.htmlformatters;

import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlNestedElement;
import pt.peddavid.yamdb.libs.http.HttpStatusCode;
import pt.peddavid.html.HtmlFactory;

public class HtmlHttpErrorFormatter implements HtmlFormatter<HttpStatusCode> {

    private final String detailedDescription;

    public HtmlHttpErrorFormatter(String description){
        this.detailedDescription = description;
    }

    @Override
    public HtmlElement supplyHtml(HttpStatusCode source) {
        HtmlNestedElement body = HtmlFactory.body().with(
                HtmlFactory.h2("HTTP ERROR: " + source.getCode()),
                HtmlFactory.p(source.getDescription())
        );
        if(detailedDescription != null){
            body.with(HtmlFactory.p(detailedDescription));
        }
        return HtmlFactory.html().with(body);
    }
}
