package pt.peddavid.yamdb.htmlformatters.util;

import pt.peddavid.html.HtmlNestedElement;

public class HtmlHyperLink extends HtmlNestedElement {

    private HtmlHyperLink(String text, String uri) {
        super("a");
        this.withText(text);
        this.setAttribute("href", uri);
    }

    /** Equivalent as the constructor but matches better with our html API*/
    public static HtmlHyperLink hyperLink(String text, String uri){
        return new HtmlHyperLink(text, uri);
    }

    /** Equivalent as the constructor but matches better with our html API*/
    public static HtmlHyperLink hyperLink(int toText, String uri){
        return new HtmlHyperLink(Integer.toString(toText), uri);
    }
}
