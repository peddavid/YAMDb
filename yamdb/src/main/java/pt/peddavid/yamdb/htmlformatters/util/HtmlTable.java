package pt.peddavid.yamdb.htmlformatters.util;

import pt.peddavid.yamdb.htmlformatters.HtmlFormatter;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.html.HtmlNestedElement;
import pt.peddavid.yamdb.util.Page;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink.hyperLink;

public class HtmlTable<E> implements HtmlFormatter<E> {

    private String title;
    private String[] headers;
    private final List<Function<E, HtmlElement>> fields = new ArrayList<>();

    public HtmlTable(){}

    public HtmlTable<E> setTitle(String title){
        this.title = title;
        return this;
    }

    public HtmlTable<E> setHeaders(String... headers){
        this.headers = headers;
        return this;
    }

    public HtmlTable<E> addHtml(Function<E, HtmlElement> field){
        fields.add(field);
        return this;
    }

    public HtmlTable<E> addString(Function<E, String> field){
        return addHtml(field.andThen(HtmlFactory::text));
    }

    public HtmlTable<E> addDate(Function<E, LocalDate> field) {
        return addHtml(field.andThen((source) -> HtmlFactory.text(source.toString())));
    }

    public HtmlTable<E> addNumeric(Function <E, ? extends Number> field) {
        return addHtml(field.andThen((source) -> HtmlFactory.text(source.toString())));
    }

    public HtmlTable<E> addInt(ToIntFunction<E> field){
        fields.add((source) -> HtmlFactory.text(Integer.toString(field.applyAsInt(source))));
        return this;
    }

    private HtmlNestedElement getStartingTable(){
        HtmlNestedElement table = HtmlFactory.table()
                .setAttribute("border", "1")
                .setAttribute("style", "width:50%")
                .with(HtmlFactory.caption(title));
        if(headers != null){
            HtmlNestedElement row = HtmlFactory.tr();
            for(String header : headers){
                row.with(HtmlFactory.th(header));
            }
            table.with(row);
        }
        return table;
    }

    @Override
    public HtmlElement supplyHtml(E source){
        return getStartingTable().with(createRows((singletonList(source))));
    }

    public HtmlElement supplyHtmlFromList(List<E> source) {
        return getStartingTable().with(createRows(source));
    }

    public HtmlElement supplyHtmlFromPage(Page<E> page){
        HtmlNestedElement table = getStartingTable();
        table.with(createRows(page.getList()));

        String save = "save=sortBy&";

        String prevPage = page.getPreviousQuery();
        String prevLink = prevPage.isEmpty() ? prevPage :
                hyperLink("&lt&lt Previous", "?" + save + prevPage).toHtml();

        String nextPage = page.getNextQuery();
        String nextLink = nextPage.isEmpty() ? nextPage :
                hyperLink("Next &gt&gt", "?" + save + nextPage).toHtml();

        /* Using the fact that HtmlElement is a functional interface
            to unite two HtmlElements without a "parent" in lambda */
        return () -> table.toHtml() + prevLink + HtmlFactory.br().toHtml() + nextLink;
    }

    public Iterable<HtmlElement> createRows(Collection<E> collection){
        return collection.stream()
                .map((element) -> HtmlFactory.tr().with(fields.stream()
                        .map((field) -> HtmlFactory.td().with(field.apply(element)))
                        .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }
}
