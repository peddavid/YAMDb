package pt.peddavid.yamdb.htmlformatters;

import kotlin.Pair;
import pt.peddavid.yamdb.htmlformatters.util.HtmlForm;
import pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink;
import pt.peddavid.yamdb.htmlformatters.util.HtmlTable;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.yamdb.model.DetailedMovie;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.model.MovieCollection;
import pt.peddavid.yamdb.model.Review;
import pt.peddavid.yamdb.util.Page;

import java.util.List;

import static pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink.hyperLink;

public class HtmlMovieFormatter {

    public static class ListMovieWithAverage implements HtmlFormatter<List<Pair<Movie, Double>>>{

        private static final HtmlTable<Pair<Movie, Double>> AVERAGE_TABLE = MovieWithAverage.getTable();

        @Override
        public HtmlElement supplyHtml(List<Pair<Movie, Double>> source) {
            return HtmlFactory.html().with(
                    HtmlFactory.body().with(
                            AVERAGE_TABLE.supplyHtmlFromList(source)
                    ),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Tops", "/tops/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static class ListMovieWithCount implements HtmlFormatter<List<Pair<Movie, Long>>>{

        private static final HtmlTable<Pair<Movie, Long>> COUNT_TABLE = MovieWithCount.getTable();

        @Override
        public HtmlElement supplyHtml(List<Pair<Movie, Long>> source) {
            return HtmlFactory.html().with(
                    HtmlFactory.body().with(
                            COUNT_TABLE.supplyHtmlFromList(source)
                    ),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Tops", "/tops/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static class MovieWithAverage implements HtmlFormatter<Pair<Movie, Double>>{

        private static final HtmlTable<Pair<Movie, Double>> AVERAGE_TABLE = getTable();

        @Override
        public HtmlElement supplyHtml(Pair<Movie, Double> source) {
            return HtmlFactory.html().with(
                    HtmlFactory.body().with(
                            AVERAGE_TABLE.supplyHtml(source)
                    ),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Tops", "/tops/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }

        public static HtmlTable<Pair<Movie, Double>> getTable(){
            return new HtmlTable<Pair<Movie, Double>>()
                    .setTitle("Movies")
                    .setHeaders("ID", "Title", "Release Year", "Added Date", "Average")
                    .addHtml((source) -> hyperLink(source.getFirst().getId(),
                            "/movies/" + source.getFirst().getId() + "/"))
                    .addString((src) -> src.getFirst().getTitle())
                    .addInt((src) -> src.getFirst().getReleaseYear())
                    .addDate((src) -> src.getFirst().getAddedDate())
                    .addString((src) -> HtmlRatingInformationFormatter
                            .getDoubleFormatter()
                            .format(src.getSecond()));
        }
    }

    public static class MovieWithCount implements HtmlFormatter<Pair<Movie, Long>>{

        private static final HtmlTable<Pair<Movie, Long>> COUNT_TABLE = getTable();

        @Override
        public HtmlElement supplyHtml(Pair<Movie, Long> source) {
            return HtmlFactory.html().with(
                    HtmlFactory.body().with(
                            COUNT_TABLE.supplyHtml(source)
                    ),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Tops", "/tops/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }

        public static HtmlTable<Pair<Movie, Long>> getTable(){
            return new HtmlTable<Pair<Movie, Long>>()
                    .setTitle("Movies")
                    .setHeaders("ID", "Title", "Release Year", "Added Date", "Count")
                    .addHtml((source) -> hyperLink(source.getFirst().getId(),
                            "/movies/" + source.getFirst().getId() + "/"))
                    .addString((src) -> src.getFirst().getTitle())
                    .addInt((src) -> src.getFirst().getReleaseYear())
                    .addDate((src) -> src.getFirst().getAddedDate())
                    .addNumeric(Pair::getSecond);
        }
    }

    public static class PageFormatter implements HtmlFormatter<Page<Pair<Movie, Double>>> {

        private static final HtmlTable<Pair<Movie, Double>> AVERAGE_TABLE = MovieWithAverage.getTable();

        private static final HtmlForm FORM = new HtmlForm("Post Movie",
                new Pair<>("Title", "title"),
                new Pair<>("Release Year", "releaseYear"))
                .action("../movies/");

        @Override
        public HtmlElement supplyHtml(Page<Pair<Movie, Double>> source) {
            String save = "save=skip,top&";
            return HtmlFactory.body().with(
                    AVERAGE_TABLE.supplyHtmlFromPage(source),
                    HtmlFactory.table().setAttribute("cellspacing", "20").with(HtmlFactory.tr().with(
                            HtmlFactory.th("Sort By: "),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("addedDate",        "?" + save + "sortBy=addedDate")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("addedDateDesc",    "?" + save + "sortBy=addedDateDesc")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("year",             "?" + save + "sortBy=year")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("yearDesc",         "?" + save + "sortBy=yearDesc")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("title",            "?" + save + "sortBy=title")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("titleDesc",        "?" + save + "sortBy=titleDesc")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("rating",           "?" + save + "sortBy=rating")),
                            HtmlFactory.td().with(HtmlHyperLink.hyperLink("ratingDesc",       "?" + save + "sortBy=ratingDesc"))
                    )),
                    FORM,
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Tops", "/tops/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static class DetailedMovieFormatter implements HtmlFormatter<DetailedMovie> {

        private static final HtmlTable<Movie> MOVIE_TABLE = HtmlMovieFormatter.getTable();
        private static final HtmlTable<MovieCollection> COLLECTION_TABLE = HtmlCollectionFormatter.getTable();
        private static final HtmlTable<Review> REVIEW_TABLE = HtmlReviewFormatter.getTable()
                .setTitle("Reviews");

        @Override
        public HtmlElement supplyHtml(DetailedMovie source) {
            Movie movie = source.getMovie();
            int id = movie.getId();
            return HtmlFactory.body().with(
                    MOVIE_TABLE.supplyHtml(source.getMovie()),
                    HtmlFactory.br(), COLLECTION_TABLE.supplyHtmlFromList(source.getCollections()),
                    HtmlFactory.br(), REVIEW_TABLE.supplyHtmlFromList(source.getReviews()),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Ratings", "/movies/" + id + "/ratings/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Reviews", "/movies/" + id + "/reviews/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Movies", "/movies/")),
                    HtmlFactory.h3().with(HtmlHyperLink.hyperLink("Home", "/"))
            );
        }
    }

    public static HtmlTable<Movie> getTable(){
        return new HtmlTable<Movie>()
                .setTitle("Movies")
                .setHeaders("ID", "Title", "Release Year", "Added Date")
                .addHtml((source) -> hyperLink(source.getId(),
                        "/movies/" + source.getId() + "/"))
                .addString(Movie::getTitle)
                .addInt(Movie::getReleaseYear)
                .addDate(Movie::getAddedDate);
    }
}
