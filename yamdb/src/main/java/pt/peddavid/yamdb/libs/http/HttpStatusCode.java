package pt.peddavid.yamdb.libs.http;

public enum HttpStatusCode {
    Ok(200, "Standard response for successful HTTP requests"),
    BadRequest(400, "Malformed request syntax or invalid request message framing"),
    NotFound(404, "The requested resource could not be found"),
    MethodNotAllowed(405, "Method is not allowed"),
    InternalServerError(500, "Server Error"),
    SeeOther(303, "See Other");

    private final int code;
    private final String description;

    HttpStatusCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static HttpStatusCode valueOf(int errorCode) {
        for(HttpStatusCode code : HttpStatusCode.values()){
            if(errorCode == code.getCode()){
                return code;
            }
        }
        return null;
    }
}
