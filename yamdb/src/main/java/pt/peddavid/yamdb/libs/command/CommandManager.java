package pt.peddavid.yamdb.libs.command;

import kotlin.Pair;
import pt.peddavid.commons.Match;
import pt.peddavid.commons.MatchResult;
import pt.peddavid.commons.NoMatch;
import pt.peddavid.commons.TreeMatcher;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static pt.peddavid.yamdb.libs.command.MatchersToAggregatorsKt.*;

public class CommandManager {
    private final Function<String, String[]> componentsExtractor;

    private final TreeMatcher<String, HttpRequest> treeMatcher = new TreeMatcher<>();
    private final Map<String, CommandPathTree> pathsByMethod = new HashMap<>();

    @SuppressWarnings("unchecked")
    public CommandManager(final Function<String, String[]> componentsExtractor){
        this.componentsExtractor = componentsExtractor;

        treeMatcher.add(getMETHOD(), getPATH());
        treeMatcher.add(getMETHOD(), getPATH(), getHEADERS());
        treeMatcher.add(getMETHOD(), getPATH(), getPARAMETERS());
        treeMatcher.add(getMETHOD(), getPATH(), getHEADERS(), getPARAMETERS());
    }

    public Pair<Command, CommandArguments> getCommand(String[] components)
            throws CommandException {
        MatchResult<HttpRequest> res = treeMatcher.get(components);
        if (res instanceof NoMatch) {
            throw new Exceptions.BadRequestException("Invalid pattern");
        }
        HttpRequest request = ((Match<HttpRequest>) res).getResult();

        String method = Objects.requireNonNull(request.getMethod()).asString();
        String path = request.getPath();

        CommandPathTree methodPaths = pathsByMethod.get(method);
        if(methodPaths == null) {
            throw new Exceptions.ResourceNotFoundException("Method \"" + method + "\" not found");
        }
        PathResult pathResult = methodPaths.get(path);
        if(pathResult == null) {
            throw new Exceptions.ResourceNotFoundException(
                    "Path \"" + path + "\" in method \"" + method + "\" " +
                    "doesn't exist or doesn't point to command"
            );
        }
        return new Pair<>(pathResult.getCommand(), new CommandArguments(request.getHeaders(),
                                                                        request.getParameters(),
                                                                        pathResult.getContext()));
    }

    public Pair<Command, CommandArguments> getCommand(String s) throws CommandException {
        return getCommand(componentsExtractor.apply(s));
    }

    public boolean addCommand(String path, Command cmd){
        MatchResult<HttpRequest> res = treeMatcher.get(componentsExtractor.apply(path));
        if (res instanceof NoMatch) {
            return false;
        }

        HttpRequest request = ((Match<HttpRequest>) res).getResult();
        String method = Objects.requireNonNull(request.getMethod()).asString();
        CommandPathTree tree = pathsByMethod.get(method);
        if (tree == null) {
            tree = new CommandPathTree();
            pathsByMethod.put(method, tree);
        }
        tree.add(request.getPath(), cmd);
        return true;
    }
}
