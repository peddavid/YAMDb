package pt.peddavid.yamdb.libs.command

import org.eclipse.jetty.http.HttpMethod
import pt.peddavid.commons.MatcherToAggregator


class HttpRequest(
        val method: HttpMethod?,
        val path: String = "",
        val headers: Map<String, String> = emptyMap(),
        val parameters: Map<String, String> = emptyMap()
)

val METHOD = MatcherToAggregator<String, HttpMethod, HttpRequest>(MethodMatcher) { args, method ->
    HttpRequest(method, args?.path ?: "", args?.headers ?: emptyMap(), args?.parameters ?: emptyMap())
}
val PATH = MatcherToAggregator<String, String, HttpRequest>(PathMatcher) { args, path ->
    HttpRequest(args?.method, path, args?.headers ?: emptyMap(), args?.parameters ?: emptyMap())
}
val HEADERS = MatcherToAggregator<String, Map<String, String>, HttpRequest>(HeadersMatcher) { args, headers ->
    HttpRequest(args?.method, args?.path ?: "", headers, args?.parameters ?: emptyMap())
}
val PARAMETERS = MatcherToAggregator<String, Map<String, String>, HttpRequest>(ParametersMatcher) { args, parameters ->
    HttpRequest(args?.method, args?.path ?: "", args?.headers ?: emptyMap(), parameters)
}
