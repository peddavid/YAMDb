package pt.peddavid.yamdb.libs.command

import pt.peddavid.commons.*

// Dataclass since equals must return different for different variableNames, otherwise
//  TreeMatcher would only create a node for the first VariableSegmentMatcher, and variables
//  would always be put in the first added "variableName"
data class VariableSegmentMatcher(private val variableName: String): Matcher<String, Pair<String, String>> {
    override fun tryMatch(key: String): MatchResult<Pair<String, String>> {
        return Match(variableName to key)
    }
}

fun variableSegment(variableName: String) =
        MatcherToAggregator(VariableSegmentMatcher(variableName)) { result: PathResult?, variable ->
            PathResult(null, result?.context?.plus(variable) ?: mapOf(variable))
        }

data class PathSegmentMatcher(private val pathSegment: String): Matcher<String, Boolean> {
    override fun tryMatch(key: String): MatchResult<Boolean> {
        if (key == pathSegment) {
            return Match(true)
        }
        return NoMatch()
    }
}

fun pathSegment(variableName: String) =
        MatcherToAggregator(PathSegmentMatcher(variableName)) { result: PathResult?, _ ->
            PathResult(null, result?.context ?: emptyMap())
        }

fun <T: Any> endNode(delegate: MatcherToAggregator<String, T, PathResult>, command: Command<*>) =
        MatcherToAggregator(delegate.matcher) { result: PathResult?, variable ->
            val delegatedResult = delegate.aggregator(result, variable)
            PathResult(command, delegatedResult.context)
        }

class PathResult(val command: Command<*>?, val context: Map<String, String> = emptyMap())
