package pt.peddavid.yamdb.libs.command;

public class Exceptions {

    public static class ResourceNotFoundException extends CommandException{
        public ResourceNotFoundException(String message) {
            super(404, message);
        }
    }

    public static class BadRequestException extends CommandException {
        public BadRequestException(String message) {
            super(400, message);
        }
    }

    public static class ServerException extends CommandException {
        private final Exception exception;
        public ServerException(Exception e){
            super(500, e.getMessage());
            this.exception = e;
        }
        @Override
        public StackTraceElement[] getStackTrace() {
            return exception.getStackTrace();
        }
    }
}
