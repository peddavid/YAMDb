package pt.peddavid.yamdb.libs.database.function;

import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

@FunctionalInterface
public interface TransactionFunction<E> {

    E apply(ConnectionService trans) throws SQLException;

}
