package pt.peddavid.yamdb.libs.command;

import pt.peddavid.commons.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static pt.peddavid.yamdb.libs.command.CommandPathTreeMatchersKt.*;

public class CommandPathTree {

    private static final String PATH_NODES_SEPARATOR = "/";

    private final TreeMatcher<String, PathResult> treeMatcher = new TreeMatcher<>();
    private Command rootCommand;

    public void add(String pathString, Command value){
        String[] path = getPath(pathString);
        if(isEmptyPath(path)) {
            rootCommand = value;
        } else {
            List<MatcherToAggregator<String, ?, PathResult>> nodes = Arrays.stream(path)
                    .map(segment -> {
                        if (isVariablePathNode(segment)) {
                            return variableSegment(segment);
                        }
                        return pathSegment(segment);
                    })
                    .collect(Collectors.toList());
            nodes.set(path.length - 1, endNode(nodes.get(path.length - 1), value));
            treeMatcher.add(nodes);
        }
    }

    public PathResult get(String pathString){
        String[] path = getPath(pathString);
        if (isEmptyPath(path)) {
            return new PathResult(rootCommand, Collections.emptyMap());
        }
        MatchResult<PathResult> result = treeMatcher.get(path);
        if (result instanceof NoMatch) {
            return null;
        }
        return ((Match<PathResult>) result).getResult();
    }

    private static String[] getPath(String path){
        return path.substring(1).split(PATH_NODES_SEPARATOR);
    }

    private static boolean isEmptyPath(String []array){
        return array.length == 1 && array[0].isEmpty();
    }

    private static boolean isVariablePathNode(String pathNode){
        // TODO: Should be configurable
        return pathNode.startsWith("{") && pathNode.endsWith("}");
    }
}
