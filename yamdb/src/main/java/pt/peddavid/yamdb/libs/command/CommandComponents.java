package pt.peddavid.yamdb.libs.command;

import org.eclipse.jetty.http.HttpMethod;
import kotlin.Pair;

import java.util.Map;
import java.util.Stack;

public class CommandComponents {

    public enum Component implements CommandComponent{
        Method(CommandComponents::methodComponent),
        Path(CommandComponents::pathComponent);

        private final CommandComponent cmd;

        Component(CommandComponent cmd){
            this.cmd = cmd;
        }

        @Override
        public boolean test(String toEvaluate){
            return cmd.test(toEvaluate);
        }
    }

    /* constants */
    private static final String HEADER_SEPARATOR = "\\|";
    private static final String ARGS_SEPARATOR = "&";
    private static final String HEADER_KEY_VALUE_SEPARATOR = ":";
    private static final String ARGS_KEY_VALUE_SEPARATOR = "=";

    /* Non instantiable class */
    private CommandComponents() { }

    private static boolean methodComponent(String method) {
        // TODO: Missing "non standard" Http Methods (e.g.: PATCH)
        return HttpMethod.fromString(method) != null;
//        if(method == null || method.length() == 0)
//            return false;
//        // 🤔🤔
//        for (int i = 0; i < method.length(); i++) {
//            if(isUpperCase(method.charAt(i))) continue;
//            return false;
//        }
//        return true;
    }

    private static boolean pathComponent(String path) {
        return !(path == null || path.length() == 0) && path.startsWith("/");
    }

    /* Components with captures */

    public static CommandComponent parametersComponentCaptureTo(final Map<String, String> capture){
        return captureKeyValue(capture, ARGS_SEPARATOR, ARGS_KEY_VALUE_SEPARATOR);
    }


    public static CommandComponent headersComponentCaptureTo(final Map<String, String> capture){
        return captureKeyValue(capture, HEADER_SEPARATOR, HEADER_KEY_VALUE_SEPARATOR);
    }

    private static CommandComponent captureKeyValue(
            final Map<String, String> capture,
            String pairsSeparator,
            String keyValueSeparator
    ){
        return (str) -> {
            /* Stack of pairs KeyValue */
            if(str == null) return false;
            // TODO: this is similar to a reduce after checking
//            List<String[]> arguments1 = Arrays.stream(str.split(pairsSeparator))
//                    .map(argument -> argument.split(keyValueSeparator))
//                    .collect(Collectors.toList());
//            if (arguments1.stream().anyMatch(element -> element.length != 2)) return false;
            // TODO: this should be a reduce, but the return value and captures are a pita
//            arguments1.forEach((pair) -> capture.put(pair[0], pair[1]));
            Stack<Pair<String, String>> stack = new Stack<>();
            String[] arguments = str.split(pairsSeparator);
            for(int i = 0; i < arguments.length; ++i){
                // TODO: This should be done before
                arguments[i] = arguments[i].replace('+', ' ');
                String[] argument = arguments[i].split(keyValueSeparator);
                if(argument.length != 2)
                    return false;
                stack.add(new Pair<>(argument[0], argument[1]));
            }
            /* Only collects pairs if the parsing was valid */
            stack.forEach((pair) -> capture.put(pair.getFirst(), pair.getSecond()));
            return true;
        };
    }

}
