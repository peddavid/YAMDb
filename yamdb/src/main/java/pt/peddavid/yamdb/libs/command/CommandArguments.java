package pt.peddavid.yamdb.libs.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;

public class CommandArguments {

    // TODO: Try to fix multiple issues in this class:
    //  * clear
    //  * clone
    //  * non private fields
    //  * demandArg should be Objects.requireNonNull

    final HashMap<String, String> headers;
    final HashMap<String, String> parameters;
    final HashMap<String, String> variables;

    public void addHeader(String key,String value){
        headers.put(key,value);
    }

    public void addParameter(String key,String value){
        parameters.put(key, value);
    }

    public void addVariables(String key,String value){
        variables.put(key,value);
    }
    public CommandArguments(){
        this(
                new HashMap<>(),
                new HashMap<>(),
                new HashMap<>()
        );
    }

    public CommandArguments(
            Map<String, String> headers,
            Map<String, String> parameters,
            Map<String, String> variables
    ){
        this.headers = new HashMap<>(headers);
        this.parameters = new HashMap<>(parameters);
        this.variables = new HashMap<>(variables);
    }

    void clear(){
        headers.clear();
        parameters.clear();
        variables.clear();
    }

    CommandArguments getClone(){
        return new CommandArguments(headers, parameters, variables);
    }

    public Optional<String> optionalHeader(String header){
        return Optional.ofNullable(headers.get(header));
    }

    public Optional<String> optionalParameter(String parameter){
        return Optional.ofNullable(parameters.get(parameter));
    }

    public Optional<String> optionalVariable(String variable){
        return Optional.ofNullable(variables.get(variable));
    }

    private String demandArg(
            String arg,
            Function<String, Optional<String>> extractor,
            ArgumentType type
    ) throws InvalidArgumentException {
        return extractor.apply(arg)
                .orElseThrow(() ->
                        new InvalidArgumentException(type, arg, " is mandatory")
                );
    }

    public String demandHeader(String header)
            throws InvalidArgumentException{
        return demandArg(header, this::optionalHeader, ArgumentType.HEADER);
    }

    public String demandParameter(String parameter)
            throws InvalidArgumentException {
        return demandArg(parameter, this::optionalParameter, ArgumentType.PARAMETER);
    }

    public String demandVariable(String variable)
            throws InvalidArgumentException {
        return demandArg(variable, this::optionalVariable, ArgumentType.VAR);
    }

    private OptionalInt demandIntArg(String arg, String result, ArgumentType type)
            throws InvalidArgumentException {
        if(result != null){
            try{
                return OptionalInt.of(Integer.parseInt(result));
            }catch (NumberFormatException e){
                throw new InvalidArgumentException(type, arg, "can't be converted to integer");
            }
        }
        throw new InvalidArgumentException(type, arg, "was not specified");
    }

    public int demandIntHeader(String header)
            throws InvalidArgumentException{
        return demandIntArg(header, headers.get(header), ArgumentType.HEADER).getAsInt();
    }

    public int demandIntParameter(String parameter)
            throws InvalidArgumentException {
        return demandIntArg(parameter, parameters.get(parameter), ArgumentType.PARAMETER).getAsInt();
    }

    public int demandIntVariable(String variable)
            throws InvalidArgumentException {
        return demandIntArg(variable, variables.get(variable), ArgumentType.VAR).getAsInt();
    }

    private Optional<Integer> optionalIntArg(
            String arg,
            Function<String, Optional<String>> extractor,
            ArgumentType type
    ) throws InvalidArgumentException {
        try {
            return extractor.apply(arg).map(Integer::valueOf);
        } catch (NumberFormatException e){
            return Optional.empty();
        }
    }

    public Optional<Integer> optionalIntHeader(String header)
            throws InvalidArgumentException {
        return optionalIntArg(header, this::optionalHeader, ArgumentType.HEADER);
    }

    public Optional<Integer> optionalIntParameter(String parameter)
            throws InvalidArgumentException {
        return optionalIntArg(parameter, this::optionalParameter, ArgumentType.PARAMETER);
    }

    public Optional<Integer> optionalIntVariable(String variable)
            throws InvalidArgumentException {
        return optionalIntArg(variable, this::optionalVariable, ArgumentType.VAR);
    }

    /* This method is mostly used for tests */
    public boolean isEmpty(){
        return headers.isEmpty() && parameters.isEmpty() && variables.isEmpty();
    }

    public enum ArgumentType{
        HEADER("Header"),
        PARAMETER("Parameter"),
        VAR("Variable");

        private final String name;
        ArgumentType(String name){
            this.name = name;
        }

        @Override
        public String toString(){
            return name;
        }
    }

    public static class InvalidArgumentException extends CommandException{
        private static final int BAD_REQUEST_ERROR_CODE = 400;

        public InvalidArgumentException(String s) {
            super(BAD_REQUEST_ERROR_CODE, s);
        }

        public InvalidArgumentException(ArgumentType type, String name, String errorMessage){
            this(type.toString() + " \"" + name + "\" " + errorMessage);
        }
    }
}
