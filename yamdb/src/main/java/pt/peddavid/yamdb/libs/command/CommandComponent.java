package pt.peddavid.yamdb.libs.command;

import java.util.function.Predicate;

@FunctionalInterface
public interface CommandComponent extends Predicate<String> {
    default CommandComponent captureTo(final String[] target){
        return (str) -> {
            boolean ret = test(str);
            if(ret) target[0] = str;
            return ret;
        };
    }
}
