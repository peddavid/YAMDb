package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.DetailedMovie;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.model.MovieCollection;
import pt.peddavid.yamdb.model.Review;
import pt.peddavid.yamdb.libs.command.Exceptions;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GetMovieCommand implements SQLCommand<DetailedMovie> {

    private final ConnectionService src;

    public GetMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public DetailedMovie executeSql(CommandArguments args) throws CommandException, SQLException {
        int mid = args.optionalIntVariable("{mid}")
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Not found"));

        return src.query(
                "select Movie.id, Movie.title, Movie.releaseYear, Movie.AddedDate, " +
                    "Rev.id, Rev.Score, Rev.name, Rev.Summary, " +
                    "Col.id, Col.Name, Col.description " +
                "from Movie " +
                "left outer join " +
                "(select Review.id, Review.MovieId, Rating.Score, Review.Name, Review.Summary " +
                    "from Review inner join Rating on Review.id = Rating.id) Rev on Movie.id = Rev.MovieID " +
                "left outer join " +
                "(select Collections.id, Collections.Name, Collections.Description, CollectionsMovie.MovieId " +
                    "from Collections inner join CollectionsMovie on " +
                    "CollectionsMovie.CollectionsId = Collections.id) Col " +
                "on Movie.id = Col.MovieId " +
                "where Movie.id = ?")
                .setInt(1, mid)
                .map(GetMovieCommand::buildFromEntireResultSet)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Movie " + mid + " not found"));
    }

    private static DetailedMovie buildFromEntireResultSet(ResultSet rs) throws SQLException{
        final Set<Review> reviews = new HashSet<>();
        final Set<MovieCollection> collections = new HashSet<>();
        final Movie movie = new Movie(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDate(4));

        do {
            Review review = new Review(rs.getInt(5), rs.getInt(1), rs.getInt(6), rs.getString(7), rs.getString(8));
            if(!rs.wasNull()){
                reviews.add(review);
            }
            MovieCollection collection = new MovieCollection(rs.getInt(9), rs.getString(10), rs.getString(11));
            if(!rs.wasNull()){
                collections.add(collection);
            }
        } while(rs.next());

        return new DetailedMovie(movie, new ArrayList<>(collections), new ArrayList<>(reviews));
    }

    @Override
    public String toString() {
        return "GetMovieCommand - Returns the movie with id {mid}";
    }
}
