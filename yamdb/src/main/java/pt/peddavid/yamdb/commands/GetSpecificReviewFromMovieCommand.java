package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Review;
import pt.peddavid.yamdb.util.RsMappers;
import pt.peddavid.yamdb.libs.command.Exceptions;

import java.sql.SQLException;

public class GetSpecificReviewFromMovieCommand implements SQLCommand<Review> {

    private final ConnectionService src;

    public GetSpecificReviewFromMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Review executeSql(CommandArguments args) throws CommandException, SQLException {
        int mid = args.demandIntVariable("{mid}");
        int rid = args.demandIntVariable("{rid}");

        return src.query(
                "select Rating.id, Rating.movieId, Rating.Score, Name, Summary, FullReview from Review " +
                    "inner join Rating on (Review.id = Rating.id) where Rating.MovieId = ? and Review.id = ?")
                .setInt(1, mid)
                .setInt(2, rid)
                .map(RsMappers::FullReview)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException(
                        "Movie " + mid + " doesn't exist or has no Review " + rid));
    }

    @Override
    public String toString() {
        return "GetSpecificReviewFromMovieCommand - " +
                "returns the full information for the review {rid} fromCollection the movie identified by {mid}";
    }
}

