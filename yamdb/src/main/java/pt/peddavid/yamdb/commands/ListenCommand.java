package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.yamdb.htmlformatters.HtmlHttpErrorFormatter;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.CommandManager;
import pt.peddavid.yamdb.libs.http.HttpResponse;
import pt.peddavid.yamdb.libs.http.HttpStatusCode;
import pt.peddavid.yamdb.util.QueryDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListenCommand implements Command<Class<Void>> {

    private static final Logger log = LoggerFactory.getLogger(ListenCommand.class);

    private static final int PAGE_SIZE = 5;

    private final ServletHandler handler;

    public ListenCommand(CommandManager manager){
        handler = new ServletHandler();

        RedirectionServlet resource = new RedirectionServlet(manager);
        ServletHolder resourceHolder = new ServletHolder(resource);
        handler.addServletWithMapping(resourceHolder, "/*");
    }

    @Override
    public Class<Void> execute(CommandArguments args) throws CommandArguments.InvalidArgumentException {
        int port = args.demandIntParameter("port");
        Server server = new Server(port);
        server.setHandler(handler);
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Void.TYPE;
    }

    private static class RedirectionServlet extends HttpServlet {

        private final CommandManager manager;
        public RedirectionServlet(CommandManager manager){
            this.manager = manager;
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doMethod(req, resp).send(resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            doMethod(req, resp).send(resp);
        }

        /* Warning because of Raw Type Command chaining functions,
         *  this should be suppressed because the we use the same command for chaining
         *  and the command has not matter the type, the command has to give something that it can format
         */
        @SuppressWarnings("unchecked")
        private HttpResponse doMethod(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            String method = req.getMethod();
            // Check if Method is allowed should be much more generic
            if(!method.equals("GET") && !method.equals("POST")){
                HtmlElement htmlError = new HtmlHttpErrorFormatter("User tried method: " + method)
                                .supplyHtml(HttpStatusCode.MethodNotAllowed);
                return new HttpResponse(HttpStatusCode.MethodNotAllowed, htmlError.toHtmlDocument());
            }
            try {
                String query = req.getQueryString();
                String referer = req.getHeader("referer");
                String[] save = req.getParameterValues("save");
                if(save != null && referer != null){
                    resp.addHeader("location", req.getRequestURI() + QueryDecoder.decode(req));
                    return new HttpResponse(HttpStatusCode.SeeOther);
                }

                String requestUri = req.getRequestURI();
                Pair<Command, CommandArguments> cmd = manager.getCommand(
                        (query == null) ? new String[]{method, requestUri }
                                        : new String[]{method, requestUri, query });
                CommandArguments args = cmd.getSecond();

                if(method.equals("GET")){
                    if(!(cmd.getFirst() instanceof GetCommandImpl)) {
                        log.error("The specified command isn't an instance of GetCommand");
                        HtmlElement htmlError = new HtmlHttpErrorFormatter("User tried method: " + method)
                                .supplyHtml(HttpStatusCode.MethodNotAllowed);
                        return new HttpResponse(HttpStatusCode.MethodNotAllowed, htmlError.toHtmlDocument());
                    } else {
                        GetCommandImpl getCommand = (GetCommandImpl) cmd.getFirst();
                        if(req.getParameterMap().get("top") == null && getCommand.getCommand() instanceof Pageable){
                            /*There is no use in redirect because it already Computed the Command.get()
                                so, just add parameter to command arguments (won't appear in the browser uri) */
                            cmd.getSecond().addParameter("top", Integer.toString(PAGE_SIZE));
                        }
                        final String content = getCommand.format(getCommand.get(args), args);
                        return new HttpResponse(HttpStatusCode.Ok, content);
                    }
                } else {
                    req.getParameterMap().forEach((key, value) -> args.addParameter(key, value[0]));
                    Object res = cmd.getFirst().execute(args);
                    resp.addHeader("Location", req.getPathInfo() + (res instanceof Number ? res + "/" : ""));
                    return new HttpResponse(HttpStatusCode.SeeOther);
                }
            } catch (CommandException e) {
                HttpStatusCode status = HttpStatusCode.valueOf(e.getErrorCode());
                HtmlElement htmlError =
                        new HtmlHttpErrorFormatter(e.getMessage()).supplyHtml(status);
                return new HttpResponse(status, htmlError.toHtmlDocument());
            }
        }
    }

    @Override
    public String toString(){
        return "ListenCommand - starts the HTTP server\n" +
                "\t port = TCP port where server should listen for requests";
    }
}
