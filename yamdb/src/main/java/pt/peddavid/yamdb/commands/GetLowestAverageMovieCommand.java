package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.Exceptions;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;

public class GetLowestAverageMovieCommand implements SQLCommand<Pair<Movie, Double>> {

    private final ConnectionService src;

    public GetLowestAverageMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Pair<Movie, Double> executeSql(CommandArguments args) throws CommandException, SQLException {
        return src.query(
                "select top 1 Movie.id, Movie.title, Movie.releaseYear, Movie.addedDate, Average " +
                "from dbo.Movie inner join (select MovieId, avg(cast(score as Float)) as Average " +
                    "from dbo.Rating group by MovieId) as R1 on(Id = R1.MovieId) order by Average")
                .map(RsMappers::MovieWithAverage)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Higher Average Movie not found"));
    }

    @Override
    public String toString() {
        return "GetLowestAverageMovieCommand - Returns the movie with lowest average";
    }
}
