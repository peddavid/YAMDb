package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class PostReviewCommand implements SQLCommand<Long> {

    private final ConnectionService src;

    public PostReviewCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Long executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        int mid = args.demandIntVariable("{mid}");
        int rating = args.demandIntParameter("rating");
        String reviewerName = args.demandParameter("reviewerName");
        String reviewSummary = args.demandParameter("reviewSummary");
        String review = args.demandParameter("review");

        return src.transaction((trans)->{
            long generatedId = trans.update("insert into Rating(Score, MovieId) values (?, ?)")
                    .setInt(1, rating)
                    .setInt(2, mid)
                    .fetchGeneratedKeys();
            trans.update("insert into Review(MovieId, id, Summary, Name, FullReview) values (?, ?, ?, ?, ?)")
                    .setInt(1, mid)
                    .setLong(2, generatedId)
                    .setString(3, reviewSummary)
                    .setString(4, reviewerName)
                    .setString(5, review)
                    .execute();

            return generatedId;
        });
    }

    @Override
    public String toString() {
        return "PostReviewCommand - creates a new review for the movie identified by {mid}, " +
                "given the following parameters\n" +
                "\treviewerName - the reviewer name\n" +
                "\treviewSummary - the review summary\n" +
                "\treview - the complete review\n" +
                "\trating - the review rating";
    }
}
