package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class PostCollectionCommand implements SQLCommand<Long> {

    private final ConnectionService src;

    public PostCollectionCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Long executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        String name = args.demandParameter("name");
        String description = args.demandParameter("description");

        return src.update("insert into Collections (name, description) values (?, ?)")
                .setString(1, name)
                .setString(2, description)
                .fetchGeneratedKeys();
    }

    public String toString(){
        return "PostCollectionCommand -  creates a new collection " +
                "and returns its identifier \n" +
                "\t name = collection name \n" +
                "\t description = collection description";
    }
}
