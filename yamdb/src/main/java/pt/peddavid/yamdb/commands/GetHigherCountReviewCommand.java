package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.Exceptions;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;

public class GetHigherCountReviewCommand implements SQLCommand<Pair<Movie, Long>> {

    private final ConnectionService src;

    public GetHigherCountReviewCommand(ConnectionService src) {
        this.src = src;
    }

    public Pair<Movie, Long> executeSql(CommandArguments args) throws CommandException, SQLException {
        return  src.query(
                "select Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate, R1.count " +
                "from Movie inner join (select top 1 MovieId, count(id) as 'count' " +
                    "from Review group by MovieId order by count desc) as R1 on(id = MovieId)")
                .map(RsMappers::MovieWithCount)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Could not find any Review"));
    }

    @Override
    public String toString() {
        return "GetHigherCountReviewCommand - Returns the movie with most reviews";
    }
}
