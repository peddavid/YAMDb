package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.RatingInformation;
import pt.peddavid.yamdb.util.RsMappers;
import pt.peddavid.yamdb.libs.command.Exceptions;

import java.sql.SQLException;

public class GetRatingCommand implements SQLCommand<RatingInformation> {

    private final ConnectionService src;

    public GetRatingCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public RatingInformation executeSql(CommandArguments args) throws CommandException, SQLException {
        int mid = args.demandIntVariable("{mid}");

        return src.query(
                "select movie.id, Average = (select avg(cast(score as Float)) " +
                "from Rating where movieId = movie.ID group by MovieId), " +
                    "\'1\' = (select count(id) from Rating where score = 1 and MovieId = movie.id), " +
                    "\'2\' = (select count(id) from Rating where score = 2 and MovieId = movie.id), " +
                    "\'3\' = (select count(id) from Rating where score = 3 and MovieId = movie.id), " +
                    "\'4\' = (select count(id) from Rating where score = 4 and MovieId = movie.id), " +
                    "\'5\' = (select count(id) from Rating where score = 5 and MovieId = movie.id) " +
                "from (select id from Movie where id = ?) movie")
                .setInt(1, mid)
                .map(RsMappers::RatingInformation)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Movie " + mid + " not found"));
    }

    @Override
    public String toString() {
        return "GetRatingCommand - Returns the rating information for the movie identified by [mid}.";
    }
}
