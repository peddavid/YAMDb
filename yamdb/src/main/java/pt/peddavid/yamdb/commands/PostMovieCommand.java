package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class PostMovieCommand implements SQLCommand<Long> {

    private final ConnectionService src;

    public PostMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Long executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        String title = args.demandParameter("title");
        int releaseYear = args.demandIntParameter("releaseYear");

        return src.update("insert into Movie (title, releaseYear) values (?, ?)")
                .setString(1, title)
                .setInt(2, releaseYear)
                .fetchGeneratedKeys();
    }

    @Override
    public String toString() {
        return "PostMovieCommand - creates a new movie, given the following parameters\n" +
                "\ttitle - movie name.\n" +
                "\treleaseYear - movie's release year.";
    }
}
