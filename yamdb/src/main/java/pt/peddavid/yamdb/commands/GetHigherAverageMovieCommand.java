package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.Exceptions;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;

public class GetHigherAverageMovieCommand implements SQLCommand<Pair<Movie, Double>> {

    private final ConnectionService src;

    public GetHigherAverageMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Pair<Movie, Double> executeSql(CommandArguments args) throws CommandException, SQLException {
         return src.query(
                 "select top 1 Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate, Average " +
                 "from Movie inner join (select MovieId, avg(cast(score as Float)) as Average  " +
                    "from Rating group by MovieId)as R1 on(Id=R1.MovieId) order by Average desc")
                 .map(RsMappers::MovieWithAverage)
                 .fetchSingleResult()
                 .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Higher Average Movie not found"));
    }

    @Override
    public String toString() {
        return "GetHigherAverageMovieCommand - Returns the movie with higher average";
    }
}
