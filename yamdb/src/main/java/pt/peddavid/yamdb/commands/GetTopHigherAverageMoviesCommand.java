package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class GetTopHigherAverageMoviesCommand implements SQLCommand<List<Pair<Movie, Double>>> {

    private final ConnectionService src;

    public GetTopHigherAverageMoviesCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public List<Pair<Movie, Double>> executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        int n = args.demandIntVariable("{n}");

        return src.query(
                "select Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate, Average " +
                "from Movie inner join (select MovieId, avg(cast(score as Float)) as Average " +
                    "from Rating group by MovieId) as R1 on(Id=R1.MovieId) order by Average desc")
                .map(RsMappers::MovieWithAverage)
                .setMaxRows(n)
                .collect(toList());
    }

    @Override
    public String toString() {
        return "GetTopHigherAverageMoviesCommand - returns a list with the {n} movies with higher average ratings, sorted decreasingly";
    }
}
