package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;

import java.util.Arrays;
import java.util.List;

public class DirectoryCommand implements Command<List<Pair<String, String>>> {

    private final List<Pair<String, String>> directoryInfo;

    @SafeVarargs
    public DirectoryCommand(Pair<String, String>... directoryInfo){
        this.directoryInfo = Arrays.asList(directoryInfo);
    }

    @Override
    public List<Pair<String, String>> execute(CommandArguments args) throws CommandArguments.InvalidArgumentException {
        return this.directoryInfo;
    }

    @Override
    public String toString(){
        return "Directory:\n\t" +
                directoryInfo.stream()
                        .map((pair) -> pair.getFirst() + " -> " + pair.getSecond())
                        .reduce((str1, str2) -> str1 + "\n\t" + str2)
                        .orElse("");
    }
}
