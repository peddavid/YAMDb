package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.model.MovieCollection;
import pt.peddavid.yamdb.libs.command.Exceptions;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetCollectionCommand implements SQLCommand<MovieCollection> {

    private final ConnectionService src;

    public GetCollectionCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public MovieCollection executeSql(CommandArguments args) throws CommandException, SQLException {
        int cid = args.demandIntVariable("{cid}");

        return src.query(
                "select Collections.id, Collections.Name, Collections.description, " +
                "Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate from dbo.Collections " +
                    "left outer join dbo.CollectionsMovie on id = CollectionsId " +
                    "left outer join dbo.Movie on CollectionsMovie.MovieId = Movie.id " +
                "where Collections.id = ?")
                .setInt(1, cid)
                .map(GetCollectionCommand::buildFromEntireResultSet)
                .fetchSingleResult()
                .orElseThrow(() -> new Exceptions.ResourceNotFoundException("Collection " + cid + " doesn't exist"));
    }

    private static MovieCollection buildFromEntireResultSet(ResultSet rs) throws SQLException{
        final int id = rs.getInt(1);
        final String title = rs.getString(2);
        final String desc = rs.getString(3);

        final List<Movie> movies = new ArrayList<>();
        do {
            Movie temp = new Movie(rs.getInt(4), rs.getString(5), rs.getInt(6), rs.getDate(7));
            if(!rs.wasNull()) {
                movies.add(temp);
            }
        } while (rs.next());

        return new MovieCollection(id, title, desc, movies);
    }

    public String toString(){
        return "GetCollectionCommand - returns the details for the cid collection," +
                " namely all the movies in that collection.";
    }
}
