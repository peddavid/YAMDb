package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;

public interface GetCommand<E> extends Command<E> {

    @Override
    default E execute(CommandArguments args) throws CommandException {
        E resource = get(args);
        display(format(resource, args), args);
        return resource;
    }

    E get(CommandArguments args) throws CommandException;
    String format(E resource, CommandArguments args) throws CommandException;
    void display(String formatted, CommandArguments args) throws CommandException;
}
