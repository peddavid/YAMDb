package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class DeleteMovieFromCollectionCommand implements SQLCommand<Class<Void>> {

    private final ConnectionService src;

    public DeleteMovieFromCollectionCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Class<Void> executeSql(CommandArguments args) throws SQLException, CommandArguments.InvalidArgumentException {
        int mid = args.demandIntVariable("{mid}");
        int cid = args.demandIntVariable("{cid}");

        src.update("delete from dbo.CollectionsMovie where MovieId = ? And CollectionsId = ?")
                .setInt(1, mid)
                .setInt(2, cid)
                .execute();
        return Void.TYPE;
    }

    public String toString(){
        return "DeleteMovieFromCollectionCommand - " +
                "removes the movie mid from the collections cid";
    }
}
