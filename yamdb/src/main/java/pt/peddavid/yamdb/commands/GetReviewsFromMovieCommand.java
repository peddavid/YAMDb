package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Review;
import pt.peddavid.yamdb.util.Page;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;
import java.util.Optional;

public class GetReviewsFromMovieCommand implements SQLCommand<Pair<Integer, Page<Review>>>, Pageable {

    private final ConnectionService src;

    public GetReviewsFromMovieCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Pair<Integer, Page<Review>> executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        Optional<Integer> skip = args.optionalIntParameter("skip");
        Optional<Integer> top = args.optionalIntParameter("top");

        int mid = args.demandIntVariable("{mid}");

        Page<Review> reviews =  src.query(
                "select Rating.id, Rating.movieId, Rating.Score, Name, Summary from Review " +
                    "inner join Rating on(dbo.Review.id = dbo.Rating.id) where dbo.Rating.MovieId = ?")
                .setInt(1, mid)
                .map(RsMappers::Review)
                .skip(skip.orElse(0))
                .top(top.map((value) -> value + 1).orElse(Integer.MAX_VALUE)) //map to + 1 so it cab check if it has more
                .collect(Page.getCollector(skip, top));
        return new Pair<>(mid, reviews);
    }

    @Override
    public String toString() {
        return "GetReviewsFromMovieCommand - Returns all the reviews for the movie identified by {mid}. " +
                "The information for each review does not include the full review text.";
    }
}
