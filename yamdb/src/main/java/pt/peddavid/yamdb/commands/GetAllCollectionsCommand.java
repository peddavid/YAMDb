package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.MovieCollection;
import pt.peddavid.yamdb.util.Page;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;
import java.util.Optional;

public class GetAllCollectionsCommand implements SQLCommand<Page<MovieCollection>>, Pageable {

    private final ConnectionService src;

    public GetAllCollectionsCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Page<MovieCollection> executeSql(CommandArguments args) throws CommandException, SQLException {
        Optional<Integer> skip = args.optionalIntParameter("skip");
        Optional<Integer> top = args.optionalIntParameter("top");

        return src.query("select Collections.id, Collections.name, Collections.description from Collections")
                .map(RsMappers::Collection)
                .skip(skip.orElse(0))
                .top(top.map((value) -> value + 1).orElse(Integer.MAX_VALUE)) //map to + 1 so it can check if it has more
                .collect(Page.getCollector(skip, top));
    }

    public String toString(){
        return "GetAllCollectionsCommand - returns the list fromCollection collections," +
                " using the insertion order";
    }
}
