package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;

public class ExitCommand implements Command<Class<Void>> {

    @Override
    public Class<Void> execute(CommandArguments args) throws IllegalArgumentException {
        System.out.println("Ending Application...");
        System.exit(0);
        return Void.TYPE;
    }

    @Override
    public String toString(){
        return "Ends the application";
    }
}
