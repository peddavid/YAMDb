package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.Exceptions;

import java.sql.SQLException;

public interface SQLCommand<E> extends Command<E> {

    @Override
    default E execute(CommandArguments args) throws CommandException {
        try{
            return executeSql(args);
        } catch (SQLException e){
           throw new Exceptions.ServerException(e);
        }
    }

    E executeSql(CommandArguments args) throws CommandException, SQLException;
}
