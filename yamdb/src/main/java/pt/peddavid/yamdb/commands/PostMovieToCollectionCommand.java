package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class PostMovieToCollectionCommand implements SQLCommand<Class<Void>> {

    private final ConnectionService src;

    public PostMovieToCollectionCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Class<Void> executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        int cid = args.demandIntVariable("{cid}");
        int mid = args.demandIntParameter("mid");

        src.update("insert into CollectionsMovie(MovieId, CollectionsId) values (?, ?)")
                .setInt(1, mid)
                .setInt(2, cid)
                .execute();

        return Void.TYPE;

    }

    @Override
    public String toString(){
        return "PostMovieToCollectionCommand - adds a movie into collection {cid}";
    }
}
