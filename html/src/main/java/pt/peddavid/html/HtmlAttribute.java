package pt.peddavid.html;

public class HtmlAttribute {

    private final String name;
    private String value;

    public HtmlAttribute(String name, String value){
        this.name = name;
        this.value = value;
    }

    public String getName(){
        return name;
    }

    public String geValue() {
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }

    public String toHtmlFormat(){
        StringBuilder builder = new StringBuilder();
        if(name != null) builder.append(" ").append(name);
        if(value != null) builder.append("=\"").append(value).append("\"");
        return builder.toString();
    }

}
