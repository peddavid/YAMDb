package pt.peddavid.html;

@FunctionalInterface
public interface HtmlElement {
    String toHtml();

    /** Appends the HTML file header */
    default String toHtmlDocument(){
        return HtmlFactory.document().toHtml() + this.toHtml();
    }
}
