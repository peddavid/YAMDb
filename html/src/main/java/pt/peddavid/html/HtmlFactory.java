package pt.peddavid.html;

public class HtmlFactory {

    public static HtmlText text(String text)                    { return new HtmlText(text); }

    public static HtmlEmptyElement document()                   { return new HtmlEmptyElement("!DOCTYPE html"); }

    public static HtmlEmptyElement tab()                        { return new HtmlEmptyElement("tab"); }
    public static HtmlEmptyElement br()                         { return new HtmlEmptyElement("br"); }
    public static HtmlEmptyElement input()                      { return new HtmlEmptyElement("input"); }

    public static HtmlNestedElement html()                      { return new HtmlNestedElement("html"); }
    public static HtmlNestedElement body()                      { return new HtmlNestedElement("body"); }

    public static HtmlNestedElement title()                     { return new HtmlNestedElement("title"); }
    public static HtmlNestedElement title(String text)          { return new HtmlNestedElement("title").withText(text); }

    public static HtmlNestedElement p()                         { return new HtmlNestedElement("p"); }
    public static HtmlNestedElement p(String text)              { return new HtmlNestedElement("p").withText(text); }
    public static HtmlNestedElement pre()                       { return new HtmlNestedElement("pre"); }
    public static HtmlNestedElement pre(String text)            { return new HtmlNestedElement("pre").withText(text); }

    public static HtmlNestedElement b()                         { return new HtmlNestedElement("b"); }
    public static HtmlNestedElement b(String text)              { return new HtmlNestedElement("b").withText(text); }

    public static HtmlNestedElement a()                         { return new HtmlNestedElement("a"); }
    public static HtmlNestedElement a(String text)              { return new HtmlNestedElement("a").withText(text); }

    public static HtmlNestedElement h1()                        { return new HtmlNestedElement("h1"); }
    public static HtmlNestedElement h1(String text)             { return new HtmlNestedElement("h1").withText(text); }
    public static HtmlNestedElement h2()                        { return new HtmlNestedElement("h2"); }
    public static HtmlNestedElement h2(String text)             { return new HtmlNestedElement("h2").withText(text); }
    public static HtmlNestedElement h3()                        { return new HtmlNestedElement("h3"); }
    public static HtmlNestedElement h3(String text)             { return new HtmlNestedElement("h3").withText(text); }
    public static HtmlNestedElement h4()                        { return new HtmlNestedElement("h4"); }
    public static HtmlNestedElement h4(String text)             { return new HtmlNestedElement("h4").withText(text); }
    public static HtmlNestedElement h5()                        { return new HtmlNestedElement("h5"); }
    public static HtmlNestedElement h5(String text)             { return new HtmlNestedElement("h5").withText(text); }

    public static HtmlNestedElement table()                     { return new HtmlNestedElement("table"); }
    public static HtmlNestedElement caption()                   { return new HtmlNestedElement("caption"); }
    public static HtmlNestedElement caption(String text)        { return new HtmlNestedElement("caption").withText(text); }
    public static HtmlNestedElement tr()                        { return new HtmlNestedElement("tr"); }
    public static HtmlNestedElement th()                        { return new HtmlNestedElement("th"); }
    public static HtmlNestedElement th(String text)             { return new HtmlNestedElement("th").withText(text); }
    public static HtmlNestedElement td()                        { return new HtmlNestedElement("td"); }
    public static HtmlNestedElement td(String text)             { return new HtmlNestedElement("td").withText(text); }

    public static HtmlNestedElement select()                    { return new HtmlNestedElement("select"); }
    public static HtmlNestedElement option()                    { return new HtmlNestedElement("option"); }
    public static HtmlNestedElement option(String text)         { return new HtmlNestedElement("option").withText(text); }

    public static HtmlNestedElement form()                      { return new HtmlNestedElement("form"); }

    public static HtmlNestedElement fieldSet()                  { return new HtmlNestedElement("fieldSet"); }

    public static HtmlNestedElement legend()                    { return new HtmlNestedElement("legend"); }
    public static HtmlElement legend(String text)              { return new HtmlNestedElement("legend").withText(text); }
}
