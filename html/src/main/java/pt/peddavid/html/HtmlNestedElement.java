package pt.peddavid.html;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HtmlNestedElement implements HtmlElement {

    private final List<HtmlElement> children = new ArrayList<>();
    private final HtmlTag tag;

    public HtmlNestedElement(String tag){
        this.tag = new HtmlTag(tag);
    }

    public HtmlNestedElement withText(String text){
        children.add(new HtmlText(text));
        return this;
    }

    public HtmlNestedElement setAttribute(String name, String value){
        tag.setAttribute(name, value);
        return this;
    }

    public HtmlNestedElement with(HtmlElement... elements){
        children.addAll(Arrays.asList(elements));
        return this;
    }

    public HtmlNestedElement with(Iterable<HtmlElement> elements){
        elements.forEach(children::add);
        return this;
    }

    @Override
    public String toHtml(){
        StringBuilder builder = new StringBuilder();
        builder.append(tag.startTag());
        for (HtmlElement elem : children) {
            builder.append(elem.toHtml());
        }
        builder.append(tag.endTag());
        return builder.toString();
    }

    @Override
    public String toString() {
        return toHtml();
    }

}
