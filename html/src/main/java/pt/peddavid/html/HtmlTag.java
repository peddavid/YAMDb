package pt.peddavid.html;

import kotlin.collections.CollectionsKt;

import java.util.ArrayList;
import java.util.List;

public class HtmlTag {

    private final List<HtmlAttribute> attributes = new ArrayList<>();
    private final String tag;

    HtmlTag(String tag){
        this.tag = tag;
    }

    /** Applies value to Attribute <name> (changes attribute in case it already existed or adds it)
     * @param name Name of the attribute
     * @param value Value to apply
     */
    void setAttribute(String name, String value) {
        HtmlAttribute attr = CollectionsKt.firstOrNull(attributes, it -> it.getName().equals(name));
        if(attr != null){
            attr.setValue(value);
        }
        attributes.add(new HtmlAttribute(name, value));
    }

    String startTag(){
        StringBuilder toRet = new StringBuilder();
        toRet.append("<").append(tag);
        for(HtmlAttribute attr : attributes){
            toRet.append(attr.toHtmlFormat());
        }
        toRet.append(">");
        return toRet.toString();
    }

    String endTag(){
        return "</" + tag + ">";
    }

}
