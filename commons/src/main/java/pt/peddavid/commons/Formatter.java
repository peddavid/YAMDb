package pt.peddavid.commons;

@FunctionalInterface
public interface Formatter<E> {
    String format(E source);
}
