package pt.peddavid.commons

import java.util.ArrayList
import java.util.function.BiPredicate

open class NTreeMap<K, UserK, V>(private val predicate: BiPredicate<K, UserK>) {

    private val tree: MutableList<ProxyNode<K, V>> = ArrayList()

    operator fun get(vararg list: UserK): V? {
        return if (list.isEmpty()) null else get(tree, 0, *list) // TODO: Is this check really needed?
    }

    @SafeVarargs
    fun newGet(vararg list: UserK): Map<K, UserK> {
        return newGet(tree, listOf(*list), mapOf())
    }

    private fun newGet(tree: List<ProxyNode<K, V>>, keys: List<UserK>, toRet: Map<K, UserK>): Map<K, UserK> {
        if (keys.isEmpty()) {
            return toRet
        }
        val (match, remainingChild) = tree.dropUntil { predicate.test(it.key, keys[0]) }
        if (match == null) {
            return emptyMap()
        }
        val childResult = newGet(match.tree, keys.drop(1), toRet.toMap().plus(match.key to keys[0]))
        return if (childResult.isNotEmpty()) childResult else newGet(remainingChild, keys, toRet)
    }

    @SafeVarargs
    private operator//TODO: Call child threes with sublist of toFind instead of carrying an index
    fun get(tree: List<ProxyNode<K, V>>, index: Int, vararg toFind: UserK): V? {
        // TODO: UserK... childToFind = toFind.sublist(1);
        var startIndex = tree.indexOfFirst { it -> predicate.test(it.key, toFind[index]) }
        /*
         * Get first valid index for tree and go search for that node
         *  if that search wasn't successful search for next valid index
         *  (next valid index is after last index, search with min = incremented startIndex
         * If we are at end of toFind we return first compatible value
         *  (a valid key is one that evaluates predicate to true and has Value
         */
        //tree.drop(startIndex).indexOfFirst{ node -> predicate.test(toFind[index], node.key) }
        while (startIndex >= 0 && startIndex < tree.size) {
            val proxy = tree[startIndex++]
            if (index == toFind.size - 1) {
                if (proxy.value != null) return proxy.value
                continue // TODO: Isn't this a return? simply return proxy.value then
            }
            val temp = get(proxy.tree, index + 1, *toFind)
            if (temp != null) return temp
            // TODO: Instead of looping call get(tree.subList(startIndex), childToFind);
            startIndex = tree
                    .drop(startIndex + 1)
                    .indexOfFirst { predicate.test(it.key, toFind[index]) }
        }
        return null
    }

    fun add(value: V, vararg list: K) {
        var currNode: ProxyNode<K, V>? = null
        var currTree: MutableList<ProxyNode<K, V>> = tree
        for (obj in list) {
            currNode = currTree.firstOrNull { it.key == obj }
            if (currNode == null) {
                currNode = ProxyNode(obj)
                currTree.add(currNode)
            }
            currTree = currNode.tree
        }
        if (currNode != null)
            currNode.value = value
    }

    fun prefixAllPaths(prefix: String): String {
        val builder = StringBuilder()
        tree.forEach { node -> builder.append(node.prefixAllPaths(prefix)) }
        return builder.toString()
    }

    override fun toString(): String {
        val builder = StringBuilder()
        tree.forEach { node -> builder.append(node.prefixAllPaths("")) }
        return builder.toString()
    }
}

fun <E> List<E>.dropUntil(predicate: (E) -> Boolean): Pair<E?, List<E>> {
    return when (val index = indexOfFirst(predicate)) {
        -1 -> null to emptyList()
        else -> this[index] to drop(index + 1)
    }
}
