package pt.peddavid.commons

import java.util.*
import java.util.Comparator.comparing
import java.util.function.ToIntBiFunction

class SortedNTreeMap<K : Comparable<K>, V> @JvmOverloads constructor(
        private val backUpPlan: ToIntBiFunction<List<K>, K> = ToIntBiFunction { tree, obj -> noBackUpPlan(tree, obj) }
) {
    companion object {
        private fun <K> noBackUpPlan(tree: List<K>, obj: K): Int {
            return -1
        }
    }

    private val tree = ArrayList<ProxyNode<K, V>>()

    @SafeVarargs
    operator fun get(vararg list: K): V? {
        var currNode: ProxyNode<K, V>? = null
        var currTree = tree
        for (obj in list) {
            currNode = currTree.getOrNull(currTree.binarySearch { it.key.compareTo(obj) })
            if (currNode == null) {
                val index = backUpPlan.applyAsInt(currTree.map(ProxyNode<K, V>::key), obj)
                if (index == -1) {
                    return null
                }
                currNode = currTree[index]
            }
            currTree = currNode.tree
        }
        return currNode?.value
    }

    @SafeVarargs
    fun add(value: V, vararg list: K) {
        var currNode: ProxyNode<K, V>? = null
        var currTree: MutableList<ProxyNode<K, V>> = tree
        for (obj in list) {
            currNode = currTree.getOrNull(currTree.binarySearch(
                    0,
                    currTree.size
            ) { it -> it.key.compareTo(obj) })
            if (currNode == null) {
                currNode = ProxyNode(obj)
                currTree.add(currNode)
                currTree.sortWith(comparing(ProxyNode<K, V>::key))
            }
            currTree = currNode.tree
        }
        if (currNode != null) {
            currNode.value = value
        }
    }

    fun prefixAllPaths(prefix: String): String {
        //        TODO:
        //        return tree.stream()
        //                .map(entry -> entry.prefixAllPaths(prefix))
        //                .collect(Collectors.joining());
        val builder = StringBuilder()
        tree.forEach { node -> builder.append(node.prefixAllPaths(prefix)) }
        return builder.toString()
    }

    override fun toString(): String {
        // TODO: return prefixAllPaths("");
        val builder = StringBuilder()
        tree.forEach { node -> builder.append(node.prefixAllPaths("")) }
        return builder.toString()
    }
}
