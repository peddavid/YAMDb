package pt.peddavid.commons

import java.util.*
import java.util.function.Predicate

open class What<K, UserK, Intermediate, V, R: Container<K, Intermediate>>(
        private val mapper: (K, UserK) -> Intermediate,
        private val predicate: Predicate<Intermediate>,
        private val emptyResult: () -> R
) {
    private val tree: MutableList<ProxyNode<K, V>> = ArrayList()

    @SafeVarargs
    operator fun get(vararg list: UserK): R {
        return get(null, tree, Arrays.asList(*list), emptyResult())
    }

    private fun get(thisProxy: ProxyNode<K, V>?, tree: List<ProxyNode<K, V>>, keys: List<UserK>, toRet: R): R {
        if (keys.isEmpty() && thisProxy?.value == null) {
            return emptyResult()
        } else if (keys.isEmpty()) {
            return toRet
        }
        val (match, remainingChild) = tree
                .map { it to mapper(it.key, keys[0]) }
                .dropUntil { (_, intermediate) -> predicate.test(intermediate) }
        if (match == null) {
            return emptyResult()
        }
        val (proxy, intermediate) = match
        @Suppress("UNCHECKED_CAST")
        val childResult = get(proxy, proxy.tree, keys.drop(1), toRet.plus(proxy.key to intermediate) as R)
        return if (childResult.isNotEmpty()) childResult else get(thisProxy, remainingChild.map { it.first }, keys, toRet)
    }

    fun add(value: V, vararg keys: K) {
        var currNode: ProxyNode<K, V>? = null
        var currTree: MutableList<ProxyNode<K, V>> = tree
        for (key in keys) {
            currNode = currTree.firstOrNull { it -> it.key == key }
            if (currNode == null) {
                currNode = ProxyNode(key)
                currTree.add(currNode)
            }
            currTree = currNode.tree
        }
        if (currNode != null)
            currNode.value = value
    }
}

interface InputTest<V> {
    fun test(input: String): TestResult
}

sealed class TestResult(val success: Boolean)
class SuccessTestResult<V>(val value: V) : TestResult(true)
class FailedTestResult : TestResult(false)

abstract class StringMapInputTest<R>(
        private val entrySeparator: Char,
        private val keyValueSeparator: Char,
        private val result: (Map<String, String>)->R
) : InputTest<R>
{
    private fun internalTest(input: String): Map<String, String>? {
        return input
                .replace('+', ' ')
                .split(entrySeparator)
                .map { argumentSplit ->
                    val keyValueSplit = argumentSplit.split(keyValueSeparator)
                    if (keyValueSplit.size != 2)
                        return null
                    val (key, value) = keyValueSplit
                    key to value
                }
                .toMap()
    }

    override fun test(input: String): TestResult = internalTest(input)
            ?.let(result)
            ?.let(::SuccessTestResult)
            ?: FailedTestResult()
}

class ParamTest: StringMapInputTest<Map<String, String>>('&', '=', {it -> it})

class HeaderTest: StringMapInputTest<HeaderResult>('|', ':', ::HeaderResult)
class HeaderResult(map: Map<String, String>): HashMap<String, String>(map)

abstract class Container<K, V> {
    operator fun plus(pair: Pair<K, V>): Container<K, V> {
        val internalPlus = this.internalPlus(pair)
        if (internalPlus == this) {
            throw IllegalStateException("Result of container plus is the same object as container: " +
                    "this is not allowed since it would keep all the results, even from backtracking attempts")
        }
        return internalPlus
    }
    /**
     * Should return always a new one, otherwise backtracked results will be kept
     */
    protected abstract fun internalPlus(pair: Pair<K, V>): Container<K, V>

    abstract fun isEmpty(): Boolean
    fun isNotEmpty() = !isEmpty()
}

class ResultContainer(
        val map: Map<InputTest<*>, SuccessTestResult<*>>
) : Container<InputTest<*>, TestResult>()
{
    override fun isEmpty() = map.isEmpty()

    override fun internalPlus(pair: Pair<InputTest<*>, TestResult>) =
            ResultContainer(map.plus(pair.first to pair.second as SuccessTestResult<*>))

    inline operator fun <reified V> get(key: InputTest<V>): V? {
        return map[key]?.value?.let(V::class.java::cast)
    }

    inline operator fun <reified V1, reified V2> get(key1: InputTest<V1>, key2: InputTest<V2>): Pair<V1?, V2?> {
        return map[key1]?.value?.let(V1::class.java::cast) to map[key2]?.value?.let(V2::class.java::cast)
    }
}