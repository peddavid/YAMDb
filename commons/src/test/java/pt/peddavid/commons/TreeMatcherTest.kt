package pt.peddavid.commons

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import pt.peddavid.commons.MatchersToAggregators.Companion.HEADERS
import pt.peddavid.commons.MatchersToAggregators.Companion.METHOD
import pt.peddavid.commons.MatchersToAggregators.Companion.nothingOf

internal class TreeMatcherTest {

    @Test
    fun getFirstMatcher() {
        val tree = TreeMatcher<String, TestHttpRequest>()

        tree.add(METHOD)

        val request = tree["GET"]

        Assertions.assertTrue(request is Match)
        Assertions.assertEquals("GET", (request as Match).result.method)
    }

    @Test
    fun getSecondMatcher() {
        val tree = TreeMatcher<String, TestHttpRequest>()

        tree.add(METHOD)
        tree.add(HEADERS)

        val request = tree["arg1=res1|arg2=res2"]

        Assertions.assertTrue(request is Match)
        Assertions.assertEquals(2, (request as Match).result.headers.size)
    }

    @Test
    fun getFromMultipleMatchers() {
        val tree = TreeMatcher<String, TestHttpRequest>()

        tree.add(METHOD, HEADERS)

        val request = tree["GET", "arg1=res1|arg2=res2"]

        Assertions.assertTrue(request is Match)
        request as Match
        Assertions.assertEquals("GET", request.result.method)
        Assertions.assertEquals(2, request.result.headers.size)
    }

    @Test
    fun `Cover cases where the "unsafe cast" could potentially cause problems`() {
        val tree = TreeMatcher<String, TestHttpRequest>()

        val message = """
            |This shows the only ways of throwing on the suppressed "Unsafe Cast" can't occur. 
            | Since the only ways it wouldn't be safe are:
            | * Matcher with nullable `PartialR`
            |   But that results in the following compilation error:
            |   > Type argument is not within its bounds: should be subtype of 'Any'
            |   e.g.: run the following line
            |   val nullableMatcher = object : Matcher<String, String?>
            | * Trying to cast a `Nothing`
            |   The only way of returning `Nothing` from the `Matcher` is by throwing (like here),
            |   which skips the cast""".trimMargin()
        tree.add(nothingOf(message))
        Assertions.assertThrows(IllegalStateException::class.java, { tree["GET"] }, message)
    }

    @Test
    fun `Can't get match from partial pattern`() {
        val tree = TreeMatcher<String, TestHttpRequest>()

        tree.add(METHOD, HEADERS)

        val request = tree["GET"]

        Assertions.assertTrue(request is NoMatch)
    }
}

object MethodMatcher: Matcher<String, String> {
    override fun tryMatch(key: String): MatchResult<String> {
        return if (key == "GET") Match("GET") else NoMatch()
    }
}

class PathMatcher: Matcher<String, String> {
    override fun tryMatch(key: String): MatchResult<String> {
        return Match("empty")
    }
}

class NothingMatcher(private val message: String): Matcher<String, Nothing> {
    override fun tryMatch(key: String): MatchResult<Nothing> {
        throw IllegalStateException(message)
    }
}

object HeadersMatcher: Matcher<String, Map<String, String>> {
    override fun tryMatch(key: String): MatchResult<Map<String, String>> {
        return Match(key.split("|").map {
            val (first, second) = it.split("=")
            first to second
        }.toMap())
    }
}

class ParametersMatcher: Matcher<String, Map<String, String>> {
    override fun tryMatch(key: String): MatchResult<Map<String, String>> {
        return Match(key.split("|").map {
            val (first, second) = it.split("=")
            first to second
        }.toMap())
    }
}

class TestHttpRequest(
        val method: String,
        val headers: Map<String, String>,
        val path: Path = Path(),
        val parameters: Map<String, String> = emptyMap()
)

fun aggregateMethod(prevResult: TestHttpRequest?, method: String) =
        TestHttpRequest(method, prevResult?.headers ?: emptyMap())

fun aggregateHeaders(prevResult: TestHttpRequest?, headers: Map<String, String>) =
        TestHttpRequest(prevResult?.method ?: "", headers)

// TODO: Investigate this. Something is strange as it shouldn't be valid
fun aggregateHeaders2(prevResult: TestHttpRequest?, headers: Map<String, String>?) =
        TestHttpRequest(prevResult?.method ?: "", headers ?: throw IllegalStateException("should not reach this"))

class Path

class MatchersToAggregators {
    companion object {
        val METHOD = MethodMatcher then ::aggregateMethod
        val HEADERS = HeadersMatcher then ::aggregateHeaders2

        fun nothingOf(message: String) =
            NothingMatcher(message) then { _: TestHttpRequest?, _ -> TestHttpRequest("Nothing", emptyMap())}
    }
}
