package pt.peddavid.commons

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.Optional
import java.util.function.BiPredicate
import java.util.function.Predicate

class NTreeMapTests {

    val pattern = NTreeMap<Predicate<String>, String, Optional<Any>>(BiPredicate(Predicate<String>::test))
    val paramTest = ParamTest()
    val headerTest = HeaderTest()
    val pattern2 = What<InputTest<*>, String, TestResult, Optional<Any>, ResultContainer>(
        InputTest<*>::test, Predicate { it -> it.success }
    ) { ResultContainer(emptyMap()) }

    val pathPattern = What<InputTest<*>, String, TestResult, Optional<Any>, ResultContainer>(
            InputTest<*>::test, Predicate { it -> it.success }
    ) { ResultContainer(emptyMap()) }

    init {
        val methodPredicate: Predicate<String> = Predicate { str -> str == "POST" }
        val pathPredicate: Predicate<String> = Predicate { str -> str.split("/").size > 1 }
        val headerPredicate: Predicate<String> = Predicate { str -> str.split("|").size > 1 }
        val paramPredicate: Predicate<String> = Predicate { str -> str.split("&").size > 1 }

        pattern.add(Optional.empty(), methodPredicate, pathPredicate)
        pattern.add(Optional.empty(), methodPredicate, pathPredicate, headerPredicate)
        pattern.add(Optional.empty(), methodPredicate, pathPredicate, paramPredicate)
        pattern.add(Optional.empty(), methodPredicate, pathPredicate, headerPredicate, paramPredicate)

        pattern2.add(Optional.empty(), paramTest)
        pattern2.add(Optional.empty(), headerTest)
        pattern2.add(Optional.empty(), paramTest, headerTest)

        pathPattern.add(Optional.empty(), StringPredicate("test"), StringPredicate("this"))
        pathPattern.add(Optional.empty(), StringPredicate("test"), IntVariablePredicate { it ->
            "get" to { test(it) }
        })
    }

    fun test(i: Int): String {
        return "Hello"
    }

    // TODO: Create a merge strategy: This one should be adding a new route (the one that comes in the resulting string
    class IntVariablePredicate<V>(private val route: (Int) -> Pair<String, () -> V>): InputTest<String> {
        val innerPath = What<InputTest<*>, String, TestResult, Optional<Any>, ResultContainer>(
                InputTest<*>::test, Predicate { it -> it.success }
        ) { ResultContainer(emptyMap()) }

        init {
            //innerPath.add()
        }

        override fun test(input: String): TestResult = input.toIntOrNull()?.let {
            FailedTestResult()
        } ?: FailedTestResult()

        fun test(input: String, vararg next: String): TestResult = input.toIntOrNull()?.let {
            val result = innerPath.get(*next)
            if (result.isNotEmpty()) { // if success and not empty
                return SuccessTestResult(null)
            }
            return FailedTestResult()
        } ?: FailedTestResult()
    }

    class StringPredicate(private val value: String): InputTest<String> {
        override fun test(input: String): TestResult {
            return if (input == value) SuccessTestResult(input) else FailedTestResult()
        }
    }

    @Test
    fun `Test all patterns`() {
        Assertions.assertNotNull(pattern.get("POST", "/"))
        Assertions.assertNotNull(pattern.get("POST", "/", "|"))
        Assertions.assertNotNull(pattern.get("POST", "/", "&"))
        Assertions.assertNotNull(pattern.get("POST", "/", "|", "&"))
    }

    @Test
    fun `Additional not accounted for patterns return null`() {
        Assertions.assertNull(pattern.get("POST", "/", "|", "&", "Not accounted for"))
    }

    @Test
    fun `Failing pattern`() {
        Assertions.assertNull(pattern.get("Not POST", "/", "|", "&"))
    }

    @Test
    fun `Change order of patterns return null`() {
        Assertions.assertNull(pattern.get("POST", "/", "&", "|"))
    }


    @Test
    fun `new Test all patterns`() {
        Assertions.assertFalse(pattern.newGet("POST", "/").isEmpty())
        Assertions.assertFalse(pattern.newGet("POST", "/", "|").isEmpty())
        Assertions.assertFalse(pattern.newGet("POST", "/", "&").isEmpty())
        Assertions.assertFalse(pattern.newGet("POST", "/", "|", "&").isEmpty())
    }

    @Test
    fun `new Additional not accounted for patterns return null`() {
        Assertions.assertTrue(pattern.newGet("POST", "/", "|", "&", "Not accounted for").isEmpty())
    }

    @Test
    fun `new Failing pattern`() {
        Assertions.assertTrue(pattern.newGet("Not POST", "/", "|", "&").isEmpty())
    }

    @Test
    fun `new Change order of patterns return null`() {
        Assertions.assertTrue(pattern.newGet("POST", "/", "&", "|").isEmpty())
    }

    @Test
    fun `test Test all patterns`() {
        val patternRes = pattern2["what=is&this=shit"]

        val paramResult: Map<String, String>? = patternRes[paramTest]
        val headerResult: Map<String, String>? = patternRes[headerTest]

        val (paramResult2, headerResult2) = patternRes[paramTest, headerTest]

        Assertions.assertNotNull(paramResult)
        Assertions.assertTrue(paramResult!!.contains("what"))
        Assertions.assertTrue(paramResult.contains("this"))

        Assertions.assertNull(headerResult)

        Assertions.assertNotNull(paramResult2)
        Assertions.assertTrue(paramResult2!!.contains("what"))
        Assertions.assertTrue(paramResult2.contains("this"))

        Assertions.assertNull(headerResult2)
    }


    @Test
    fun `test Test all patterns 2`() {
        val patternRes = pattern2["what=is&this=shit", "this:header|other:header2"]

        val paramResult: Map<String, String>? = patternRes[paramTest]
        val headerResult: HeaderResult? = patternRes[headerTest]

        Assertions.assertNotNull(paramResult)
        Assertions.assertTrue(paramResult!!.contains("what") && paramResult["what"] == "is")
        Assertions.assertTrue(paramResult.contains("this") && paramResult["this"] == "shit")

        Assertions.assertNotNull(headerResult)
        Assertions.assertTrue(headerResult!!.contains("this") && headerResult["this"] == "header")
        Assertions.assertTrue(headerResult.contains("other") && headerResult["other"] == "header2")
    }

    @Test
    fun `test path pattern`() {
        val patternRes = pathPattern["test", "this"]

        Assertions.assertTrue(patternRes.isNotEmpty())

        val patternFailRes1 = pathPattern["test", "this", "doesn't exist"]
        Assertions.assertTrue(patternFailRes1.isEmpty())

        val patternFailRes2 = pathPattern["test", "doesn't exist"]
        Assertions.assertTrue(patternFailRes2.isEmpty())

        val patternFailRes3 = pathPattern["test"]
        Assertions.assertTrue(patternFailRes3.isEmpty())

        val patternFailRes4 = pathPattern["test", "2"]
        Assertions.assertTrue(patternFailRes4.isEmpty())
    }

    fun routes(inner: () -> Unit) {}

    fun <V> get(path: String, inner: () -> V) {}
    fun <V> route(path: String, inner: () -> V) = { }

    fun <V> int(inner: (Int) -> V) = { inner(1) }
    fun <V> uuid(inner: (String) -> V) = { inner("") }

    class IntVariableTester<V>(val myInner: (Int) -> V) {
        fun test(str: String): () -> V {
            return { myInner(str.toInt()) }
        }
    }

    @Test
    fun `wanted usage`() {
        routes {
            get("this/some/thing") {
                "Result OK!"
            }

            // Although the following strategy might seem the cleaner, as in:
            // - very type safe
            // - doesn't require a "monolith" context object that holds all results
            // This solution is completely lazy, and as such it can only build the tree at runtime
            // Trying to compile this eagerly with default values (kinda hacky) would result in a lambda capture with
            // that default value, what would restrict the usage to much and / or lead to tricky bugs
            // (e.g.: the parameter couldn't be used in the built tree, or else that tree couldn't be built):
            get("item", int{ id -> route(
                    /* This couldn't be precomputed -> */"details/$id",
                    uuid { uuid -> "Result Ok for $id details with $uuid" })})

            get("this/some/thing", int{ id -> "Result OK for $id"})
            get("item", int{ id -> route("details") { "Result Ok for $id details" }})
            get("item", int{ id -> route("details", uuid { uuid -> "Result Ok for $id details with $uuid" })})

            route("item") {
                int { id ->
                    get("details") {
                        "Result Ok items $id details"
                    }
                }
            }
        }
    }
}
